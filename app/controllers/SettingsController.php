<?php

class SettingsController extends \BaseController {
	const EUVALDO = 'euv';
	const GROUP = 'grp';
	const PROJECT = 'pro';
	const CURRENT_ID = "current_id";
	const BANNER_PATH = "/img/helper/banner/";
	const BANNER_1 = "b1";
	const BANNER_2 = "b2";
	const BANNER_3 = "b3";

	private $currentUserId;

	/**
	 * Display a listing of the resource.
	 * GET /settings
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$project = About::whereType(self::PROJECT)->first();
		$group = About::whereType(self::GROUP)->first();
		$euvaldo = About::whereType(self::EUVALDO)->first();
		$poem = Poem::first();
		$banner1 = Banner::whereKey(self::BANNER_1)->first();
		$banner2 = Banner::whereKey(self::BANNER_2)->first();
		$banner3 = Banner::whereKey(self::BANNER_3)->first();
		return View::make('admin.settings', ['project'=>$project, 'group'=>$group, 'euvaldo'=>$euvaldo, 'poem'=>$poem, 'banner1'=>$banner1, 'banner2'=>$banner2, 'banner3'=>$banner3]);
	}

	public function SettingsController() {
		$encryptedUserId = Session::get(self::CURRENT_ID);
		if ($encryptedUserId) {
			$this->currentUserId = Crypt::decrypt($encryptedUserId);
		} else {
			$this->currentUserId = "";
		}
	}

	public function AnySavePoem() {
		$quotation = Input::get('quotation');
		$quotationAuthor = Input::get('quotation-author');
		$introduction = Input::get('introduction');
		$title = Input::get('title');
		$text = Input::get('text');
		$author = Input::get('author');

		$poem = Poem::first();
		if ($poem == null) {
			$poem = new Poem;
		}

		$poem->quotation = $quotation;
		$poem->quotation_author = $quotationAuthor;
		$poem->introduction = $introduction;
		$poem->title = $title;
		$poem->text = $text;
		$poem->author = $author;
		$poem->save();

		if ($poem) {
			$result = ['success'=>true, "poem"=>$poem];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	public function AnySaveEuvaldo() {
		$introduction = Input::get('introduction');
		$text = Input::get('text');

		$euvaldo = About::whereType(self::EUVALDO)->first();
		if ($euvaldo == null) {
			$euvaldo = new About;
		}

		$euvaldo->type = self::EUVALDO;
		$euvaldo->introduction = $introduction;
		$euvaldo->text = $text;
		$euvaldo->save();

		if ($euvaldo) {
			$result = ['success'=>true, "euvaldo"=>$euvaldo];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	public function AnySaveProject() {
		$groupIntroduction = Input::get('group-introduction');
		$groupText = Input::get('group-text');
		$projectIntroduction = Input::get('project-introduction');
		$projectText = Input::get('project-text');

		$group = About::whereType(self::GROUP)->first();
		if ($group == null) {
			$group = new About;
		}

		$group->type = self::GROUP;
		$group->introduction = $groupIntroduction;
		$group->text = $groupText;
		$group->save();

		$project = About::whereType(self::PROJECT)->first();
		if ($project == null) {
			$project = new About;
		}

		$project->type = self::PROJECT;
		$project->introduction = $projectIntroduction;
		$project->text = $projectText;
		$project->save();

		if ($group && $project) {
			$result = ['success'=>true, "group"=>$group, "project"=>$project];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	public function AnySaveBanner() {
		$title1 = Input::get('banner-title-1');
		$subtitle1 = Input::get('banner-subtitle-1');
		$link1 = Input::get('banner-link-1');
		$title2 = Input::get('banner-title-2');
		$subtitle2 = Input::get('banner-subtitle-2');
		$link2 = Input::get('banner-link-2');
		$title3 = Input::get('banner-title-3');
		$subtitle3 = Input::get('banner-subtitle-3');
		$link3 = Input::get('banner-link-3');

		//return Input::file( 'banner_file_3' ) -> getFileName();

		if (Input::hasFile('banner_file_1')) {
			$file1 = Input::file('banner_file_1');
			$v1 = $this->validateImage($file1, "b1.jpg");
			//return ['validate'=>$v1];
			//return $file1;
		}
		if (Input::hasFile('banner_file_2')) {
			$file2 = Input::file('banner_file_2');
			$v2 = $this->validateImage($file2, "b2.jpg");
			//return $file2;
		}
		if (Input::hasFile('banner_file_3')) {
			$file3 = Input::file('banner_file_3');
			$v3 = $this->validateImage($file3, "b3.jpg");
			//return $file3;
		}

		//return ["hasfile"=>Input::hasFile('banner_file_1'), "file"=> $file1];

		$banner1 = Banner::whereKey(self::BANNER_1)->first();
		if (!$banner1) $banner1 = new Banner();
		$banner1->title = $title1;
		$banner1->subtitle = $subtitle1;
		$banner1->link = $link1;
		$banner1->key = self::BANNER_1;
		$banner1->save();

		$banner2 = Banner::whereKey(self::BANNER_2)->first();
		if (!$banner2) $banner2 = new Banner();
		$banner2->title = $title2;
		$banner2->subtitle = $subtitle2;
		$banner2->link = $link2;
		$banner2->key = self::BANNER_2;
		$banner2->save();

		$banner3 = Banner::whereKey(self::BANNER_3)->first();
		if (!$banner3) $banner3 = new Banner();
		$banner3->title = $title3;
		$banner3->subtitle = $subtitle3;
		$banner3->link = $link3;
		$banner3->key = self::BANNER_3;
		$banner3->save();
		
		if ($v1 && $v2 && $v3) {
			$result = ['success'=>true, "banner1"=>$banner1, "banner2"=>$banner2, "banner3"=>$banner3];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	private function validateImage($file, $filename) {
		//$files = Input::file('images');
		$rules = ['file' => 'required|image'];
		$destinationPath = public_path() . self::BANNER_PATH;
		//return $destinationPath;
		$count = 0;

		$validator = Validator::make(['file'=>$file], $rules);
		//return "validator passes:" . $validator->passes();
		if ($validator->passes()) {
			$originalfilename = $file->getClientOriginalName();
			//$fileextension = File::extension($originalfilename);
			// move from /temp to your directory
			$upload_success = $file->move($destinationPath, $filename);

			if ($upload_success) {
				// generate a thumbnail
				//$thumbnail = $this->createThumbnail($destinationPath, $filename);
				/* just for test puposes*/
				$done[] = $filename;
				$uploaded[] = array(
				'name' => $filename,
				'file' => self::BANNER_PATH . $filename
				);

				return true;
			} else {
				$filename = $file->getClientOriginalName();
			    $not[] = $filename;
			    return false;
			}
		}
	}

	public function showPoem() {
		$poem = Poem::first();
		return View::make('public.poems')->with('poem', $poem);
	}

	public function showEuvaldo() {
		$euvaldo = About::whereType(self::EUVALDO)->first();
		return View::make('public.biografy')->with('euvaldo', $euvaldo);
	}

	public function showProject() {
		$project = About::whereType(self::PROJECT)->first();
		$group = About::whereType(self::GROUP)->first();
		return View::make('public.project', ['project'=>$project, 'group'=>$group]);
	}

}