<?php

class PhotoController extends \BaseController {
	const FRONT_PATH = '/img/photos/front/';
	const BACK_PATH = '/img/photos/back/';
	const BANNER_1 = "b1";
	const BANNER_2 = "b2";
	const BANNER_3 = "b3";

	/**
	 * Display a listing of the resource.
	 * GET /photo
	 *
	 * @return Response
	 */

	public function getIndex() {
		$photos = Photo::orderBy('updated_at','desc')->get();

		if ($photos) {
			$result = ['success'=>true, 'photos'=>$photos];
		} else {
			$result = ['success'=>false];
		}

		return View::make('admin.home', $result);
	}

	public function dashboardAmount() {
		$photos = Photo::count();
		$negatives = Negative::count();
		$slides = Slide::count();
		$writings = Writing::count();
		$photosPending = Photo::where('pending', '<>', 0)->count();
		$slidesPending = Negative::where('pending', '<>', 0)->count();
		$writingsPending = Slide::where('pending', '<>', 0)->count();
		$negativesPending = Writing::where('pending', '<>', 0)->count();

		return View::make('admin.dashboard', [
			'photoamount'=>$photos, 'negativeamount'=>$negatives, 'slideamount'=>$slides, 'writingamount'=>$writings,
			'pendingphotos'=>$photosPending, 'pendingslides'=>$slidesPending, 'pendingwritings'=>$writingsPending, 'pendingnegatives'=>$negativesPending]);
	}

	// Dashboard edit pictures
	public function photoEdit() {
		$photo = Photo::whereCode(Input::get("code"))->first();
		$photo->code = Input::get("code");
		$photo->back_note = Input::get("back_note");
		$photo->color = Input::get("color");
		$photo->conservation = Input::get("conservation");
		$photo->edge = Input::get("edge");
		$photo->executed_treatment = Input::get("executed_treatment");
		$photo->framing = Input::get("framing");
		$photo->front_note = Input::get("front_note");
		$photo->geo_location = Input::get("geo_location");
		$photo->height = Input::get("height");
		$photo->intervention = Input::get("intervention");
		$photo->label = Input::get("label");
		$photo->observation = Input::get("observation");
		$photo->pending = Input::get("pending");
		$photo->published_at = Input::get("published_at");
		$photo->series = Input::get("series");
		$photo->suggested_treatment = Input::get("suggested_treatment");
		$photo->title = Input::get("title");
		$photo->width = Input::get("width");
		$photo->save();
		if ($photo) {
			$result = ['success'=>true, "media"=>$photo];
		} else {
			$result = ['success'=>false];
		}
		return $result;
	}

	public function getByCode() {
		$code = Input::get("code");
		$photo = Photo::whereCode($code)->first();
		if ($photo) {
			$result = ['success'=>true, 'media'=>$photo];
		} else {
			$result = ['success'=>false];
		}
		return $result;
	}

	public function amount() {
		$photos = Photo::count();
		$negatives = Negative::count();
		$slides = Slide::count();
		$writings = Writing::count();
		$poem = Poem::first();
		$banner1 = Banner::whereKey(self::BANNER_1)->first();
		$banner2 = Banner::whereKey(self::BANNER_2)->first();
		$banner3 = Banner::whereKey(self::BANNER_3)->first();
		$banners = [];
		$banners[] = $banner1;
		$banners[] = $banner2;
		$banners[] = $banner3;

		return View::make('public.home', ['photoamount'=>$photos, 'negativeamount'=>$negatives, 'slideamount'=>$slides, 'writingamount'=>$writings, 'poem'=>$poem, 'banners'=>$banners]);
	}

	public function show($id) {
		$photo = Photo::find($id);
		$amount = Photo::count();
		return View::make('public.show',['photo' => $photo, 'amount'=>$amount, 'type' => 'photos']);
	}

	public function anyUpload() {
		$files = array();

		if (Input::hasFile('images')) {
			$files = Input::file('images');
			$rules = ['file' => 'required|image'];
			$destinationPath = public_path().self::FRONT_PATH;

			foreach ($files as $file) {
				$validator = Validator::make(['file'=>$file], $rules);
				if ($validator->passes()) {
					$filename = $file->getClientOriginalName();
		            // move from /temp to your directory
					$upload_success = $file->move($destinationPath, $filename);
					if ($upload_success) {
						// generate a thumbnail
						$thumbnail = $this->createThumbnail($destinationPath, $filename);
						/* just for test puposes*/
						$done[] = $filename;
						//Session::flash('done', $done);
						$uploaded[] = array(
						'name' => $filename,
						'file' => self::FRONT_PATH . $filename
						);
					} else {
						$filename = $file->getClientOriginalName();
	                    $not[] = $filename;
	                    //Session::flash('not', $not);
					}
				} else {
					return Redirect::back()->withErrors($validator);
				}
			}
		} else {
			return Redirect::back()->withErrors('choose a file');
		}

		return Response::json($uploaded);		
	}

	public function anyAdd(){
		$photo = new Photo;
		$this->setPhotoAttributes($photo);
		$photo->save();

		if ($photo) {
			$result = ['success'=>true, 'photo'=>$photo];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	public function anyEdit() {
		$photo = Photo::find(Input::get('photoId'));

		if ($photo) {
			$this->setPhotoAttributes($photo);
			$photo->save();
			$result = ['success'=>true, 'photo'=>$photo];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	public function anyRemove() {
		$photo = Photo::find(Input::get('photoId'));

		if ($photo) {
			$this->deleteDeteriorations($photo);
			$this->deletePublications($photo);
			$photo->delete();
			$result = ['success'=>true];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	private function deleteDeteriorations($photo) {
		$deteriorations = Deterioration::find($photo->id);

		foreach ($deteriorations as $deterioration) {
			$deterioration->delete();
		}
	}

	private function deletePublications($photo) {
		$publications = Publication::find($photo->id);

		foreach ($publications as $publication) {
			$publication->delete();
		}
	}

	/* 
	** Retreive 30 elements at a time
	** var amount = 30;
	** var index = 0, 30, 60, 70...
	*/
	public function anyAll() {
		$index = Input::get('index');
		$amount = 30;
		$start = $index - $amount;
		$photos = Photo::orderBy('updated_at','desc')->skip($start)->take($amount)->get();

		if ($photos) {
			$result = ['success'=>true, 'photos'=>$photos];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	public function anySearch() {
		$attribute = Input::get('attribute');
		$text = Input::get('text');
		$index = Input::get('index');
		$amount = 30;
		$start = $index - $amount;
		//$start = 0;
		$photos = Photo::where($attribute,'LIKE','%'.$text.'%')
			->orderBy('updated_at','desc')
			->skip($start)
			->take($amount)
			->get();

		if ($photos) {
			$result = ['success'=>true, 'photos'=>$photos];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	private function setPhotoAttributes($photo) {
		// Not null attributes.
		$photo->code = Input::get('code');
		$photo->label = Input::get('label');
		$photo->title = Input::get('title');

		if (Input::hasFile('front_path')) {
			Input::file('front_path')
				->move(public_path().'/img/photos/front',Input::file('front_path')->getClientOriginalName());
			$photo->front_path =  self::image_base_url.'/img/photos/front/'.Input::file('front_path')->getClientOriginalName();
		} else {
			$photo->front_path = "path";
		}

		if (Input::hasFile('back_path')) {
			Input::file('back_path')
			->move(public_path().'/img/photos/verse',Input::file('back_path')->getClientOriginalName());
			$photo->back_path =  self::image_base_url.'/img/photos/verse/'.Input::file('back_path')->getClientOriginalName();
		} else {
			$photo->back_path = "path";
		}
		
		$photo->front_file_name = Input::get('front_file_name');
		$photo->back_file_name = Input::get('back_file_name');
		// Null attributes.
		$photo->width = Input::get('width');
		$photo->height = Input::get('height');
		$photo->framing = Input::get('framing');
		$photo->conservation = Input::get('conservation');
		$photo->front_note = Input::get('front_note');
		$photo->back_note = Input::get('back_note');
		$photo->suggested_treatment = Input::get('suggested_treatment');
		$photo->executed_treatment = Input::get('executed_treatment');
		$photo->observation = Input::get('observation');
		$photo->keeper = Input::get('keeper');
		$photo->intervention =  Input::get('intervention');
		$photo->published_at = Input::get('published_at');
	}

	public function anyAddDeterioration(){
		$photo = Photo::find(Input::get('photoId'));
		$characteristic = Input::get('characteristic');

		if ($photo && $characteristic != '') {
			$deterioration = new Deterioration;
			$deterioration->photo = $photo->id;
			$deterioration->characteristic = $characteristic;
			$deterioration->save();

			if ($deterioration) {
				$result = ['success'=>true, 'deterioration'=>$deterioration];
			} else {
				$result = ['success'=>false];
			}
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	public function anyAddPublication(){
		$photo = Photo::find(Input::get('photoId'));

		if ($photo) {
			$publication = new Publication;
			$publication->photo = $photo->id;
			$publication->type = Input::get('type');
			$publication->name = Input::get('name');
			$publication->save();

			if ($publication) {
				$result = ['success'=>true, 'publication'=>$publication];
			} else {
				$result = ['success'=>false];
			}
		} else {
			$result = ['success'=>false];
		}
	}

}