<?php

class NegativeController extends \BaseController {
	const PATH = "/img/negatives";

	/**
	 * Display a listing of the resource.
	 * GET /negative
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return 'You are in negative!';
	}

	public function show($id) {
		$negative = Negative::find($id);
		$amount = Negative::count();
		return View::make('public.show',['negative' => $negative, 'amount'=>$amount, 'type' => 'negatives']);
	}

	public function anyUpload() {
		$files = array();

		if (Input::hasFile('images')) {
			$files = Input::file('images');
			$rules = ['file' => 'required|image'];
			$destinationPath = public_path().self::PATH;

			foreach ($files as $file) {
				$validator = Validator::make(['file'=>$file], $rules);
				if ($validator->passes()) {
					$filename = $file->getClientOriginalName();
		            // move from /temp to your directory
					$upload_success = $file->move($destinationPath, $filename);
					if ($upload_success) {
						// generate a thumbnail
						$thumbnail = $this->createThumbnail($destinationPath, $filename);
						/* just for test puposes*/
						$done[] = $filename;
						Session::flash('done', $done);
						$uploaded[] = array(
						'name' => $filename,
						'file' => self::PATH . $filename
						);
					} else {
						$filename = $file->getClientOriginalName();
	                    $not[] = $filename;
	                    Session::flash('not', $not);
					}
				} else {
					return Redirect::back()->withErrors($validator);
				}
			}
		} else {
			return Redirect::back()->withErrors('choose a file');
		}

		return Response::json($uploaded);		
	}

	public function anyAdd(){
		$negative = new Negative;
		$this->setNegativeAttributes($negative);
		$negative->save();

		if ($negative) {
			$result = ['success'=>true, 'negative'=>$negative];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	public function anyEdit() {
		$negative = Negative::find(Input::get('negativeId'));

		if ($negative) {
			$this->setNegativeAttributes($negative);
			$negative->save();
			$result = ['success'=>true, 'negative'=>$negative];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	public function anyRemove() {
		$negative = Negative::find(Input::get('negativeId'));

		if ($negative) {
			$negative->delete();
			$result = ['success'=>true];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	/* 
	** Retreive 30 elements at a time
	** var amount = 30;
	** var index = 0, 30, 60, 70...
	*/
	public function anyAll() {
		$index = Input::get('index');
		$amount = 30;
		$start = $index - $amount;
		$negatives = Negative::orderBy('updated_at','desc')->skip($start)->take($amount)->get();

		if ($negatives) {
			$result = ['success'=>true, 'negatives'=>$negatives];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	public function anySearch() {
		$attribute = Input::get('attribute');
		$text = Input::get('text');
		$index = Input::get('index');
		$amount = 30;
		$start = $index - $amount;
		//$start = 0;
		$negatives = Negative::where($attribute,'LIKE','%'.$text.'%')
			->orderBy('updated_at','desc')
			->skip($start)
			->take($amount)
			->get();

		if ($negatives) {
			$result = ['success'=>true, 'negatives'=>$negatives];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	private function setNegativeAttributes($negative) {
		// Not null attributes.
		$negative->code = Input::get('code');

		if (Input::hasFile('path')) {
			Input::file('path')
				->move(public_path().'\img\negatives',Input::file('path')->getClientOriginalName());
			$negative->path = self::image_base_url.'/img/negatives/'.Input::file('path')->getClientOriginalName();
		} else {
			$negative->path = "path";
		}

		// Null attributes.
		$negative->collection = Input::get('collection');
		$negative->number = Input::get('number');
		$negative->description = Input::get('description');
		$negative->back_note = Input::get('back_note');
		$negative->geo_location = Input::get('geo_location');
		$negative->conservation = Input::get('conservation');
		$negative->observation = Input::get('observation');
		$negative->brand = Input::get('brand');
		$negative->color = Input::get('color');
		$negative->intervention_date = Input::get('intervention_date');
	}

}