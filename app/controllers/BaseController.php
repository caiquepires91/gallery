<?php

class BaseController extends Controller {
	const currentUserId = 'currentUserId';
	const image_base_url = 'http://localhost/gallery/public';	

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	protected function createThumbnail($destinationPath, $filename) {
		$image = Image::make($destinationPath.'/'.$filename);
		// get image to right orientation
		$image->orientate();
		// fit into 300, 240 fram best way
		$image->fit(300, 240, function ($constraint) {
			$constraint->upsize();
		});
		$image->save($destinationPath.'/thumbnails'.'/'.$filename);
		
		return $image;
	}

}
