<?php

class UserController extends \BaseController {
	const CURRENT_ID = "current_id";
	private $currentUserId;

	/**
	 * Display a listing of the resource.
	 * GET /user
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return "You're in User";
	}

	public function UserController(){
		$encryptedUserId = Session::get(self::CURRENT_ID);
		if ($encryptedUserId) {
			$this->currentUserId = Crypt::decrypt($encryptedUserId);
		} else {
			$this->currentUserId = "";
		}
	}

	public function anyRegister(){
		$user = User::whereEmail(Input::get('email'))->first();

		if ($user) {
			// User has already been registered before.
			$result = ['success'=>false];
		} else {
			$user = new User;
			$user->email = Input::get('email');
			$user->password = Hash::make(Input::get('password'));
			$user->name = Input::get('name');
			$user->save();
			$result = ['success'=>true, 'user'=>$user];
		}

		return $result;
	}

	public function anyLogin(){
		$user = User::whereEmail(Input::get('email'))->first();

		if ($user) {
			// Email exists.
			if (Hash::check(Input::get('password'), $user->password)) {
				// Password correct.
				Session::put(self::currentUserId, Crypt::encrypt($user->id));
				$result = ['success'=>true, 'user'=>$user];
			} else {
				// Password incorrect.
				$result = ['success'=>false];
			}
		} else {
			// Email incorrect.
			$result = ['success'=>false];
		}

		return $result;

	}

	/**
	 * Show the form for creating a new resource.
	 * GET /user/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /user
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /user/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /user/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /user/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /user/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}