<?php

class WritingController extends \BaseController {
	const WRITINGS_PATH = '/img/writings';
	const ILLUSTRATIONS_PATH = '/img/illustrations';

	/**
	 * Display a listing of the resource.
	 * GET /writing
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return 'You are in Writings';
	}

	public function show($id) {
		$writing = Writing::find($id);
		$amount = Writing::count();
		return View::make('public.show',[
			'writing' => $writing,
			'amount'=>$amount,
			'type' => 'writings',
			'section'=>$writing->section]);
	}

	public function anyUpload() {
		$files = array();

		if (Input::hasFile('images')) {
			$files = Input::file('images');
			$rules = ['file' => 'required|image'];
			$destinationPath = public_path().self::WRITINGS_PATH;

			foreach ($files as $file) {
				$validator = Validator::make(['file'=>$file], $rules);
				if ($validator->passes()) {
					$filename = $file->getClientOriginalName();
		            // move from /temp to your directory
					$upload_success = $file->move($destinationPath, $filename);
					if ($upload_success) {
						// generate a thumbnail
						$thumbnail = $this->createThumbnail($destinationPath, $filename);
						/* just for test puposes*/
						$done[] = $filename;
						Session::flash('done', $done);
						$uploaded[] = array(
						'name' => $filename,
						'file' => self::WRITINGS_PATH . $filename
						);
					} else {
						$filename = $file->getClientOriginalName();
	                    $not[] = $filename;
	                    Session::flash('not', $not);
					}
				} else {
					return Redirect::back()->withErrors($validator);
				}
			}
		} else {
			return Redirect::back()->withErrors('choose a file');
		}

		return Response::json($uploaded);		
	}

	public function anyAdd() {
		$writing = new Writing;
		$this->setWritingAttributes($writing);
		$writing->save();
		$this->addIllustration($writing);

		if ($writing) {
			$result = ['success'=>true, 'writing'=>$writing];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	public function anyEdit() {
		$writing = Writing::find(Input::get('writingId'));

		if ($writing) {
			$this->setWritingAttributes($writing);
			$writing->save();
			$result = ['success'=>true, 'writing'=>$writing];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	public function anyRemove() {
		$writing = Writing::find(Input::get('writingId'));

		if ($writing) {
			$this->deleteIllustrations($writing);
			$writing->delete();
			$result = ['success'=>true];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	private function deleteIllustrations($writing) {
		$illustrations = Illustration::whereWriting($writing->id)->get();

		foreach ($illustrations as $illustration) {
			$illustration->delete();
		}
	}

	
	/* 
	** Called to list sections
	** Retreive 30 elements at a time
	** var amount = 30;
	** var index = 0, 30, 60, 70...
	*/
	public function anyAll() {
		$index = Input::get('index');
		$amount = 30;
		$start = $index - $amount;
		$writings = Writing::orderBy('updated_at','desc')
			->groupBy('section')
			->skip($start)
			->take($amount)
			->get();

		if ($writings) {
			$result = ['success'=>true, 'writings'=>$writings];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	// show sections that contains the text searched.
	public function anySearch() {
		$attribute = Input::get('attribute');
		$text = Input::get('text');
		$index = Input::get('index');
		$amount = 30;
		$start = $index - $amount;
		//$start = 0;
		$writings = Writing::where($attribute,'LIKE','%'.$text.'%')
			->orderBy('updated_at','desc')
			->groupBy('section')
			->skip($start)
			->take($amount)
			->get();

		if ($writings) {
			$result = ['success'=>true, 'writings'=>$writings];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	// called when a folder (section) is clicked.
	public function getWritingsBySection($section) {
		$index = Input::get('index');
		$amount = 30;
		return $this->getBySection($index, $amount, $section);
	}

	// get all writhings of a specific section.
	public function anyAllWritings() {
		$index = Input::get('index');
		$amount = 30;
		$section = Input::get('section');
		//return ['index'=>$index, 'amount'=>$amount, 'section'=>$section];
		return $this->getBySection($index, $amount, $section);
	}

	private function getBySection($index, $amount, $section) {
		$start = $index - $amount;
		$writings = Writing::whereSection($section)
			->orderBy('updated_at','desc')
			->skip($start)
			->take($amount)
			->get();

		if ($writings) {
			$result = ['success'=>true, 'writings'=>$writings,'type'=>'sections', 'section'=>$section];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	// search in all writings of a specific section.
	public function anySearchWritings() {
		$section = Input::get('section');
		$attribute = Input::get('attribute');
		$text = Input::get('text');
		$index = Input::get('index');
		$amount = 30;
		$start = $index - $amount;
		//$start = 0;
		$writings = Writing::whereSection($section)
			->where($attribute,'LIKE','%'.$text.'%')
			->orderBy('updated_at','desc')
			->groupBy('section')
			->skip($start)
			->take($amount)
			->get();

		if ($writings) {
			$result = ['success'=>true, 'writings'=>$writings];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}


	private function setWritingAttributes($writing) {
		// Not null attributes.
		$writing->code = Input::get('code');

		if (Input::hasFile('path')) {
			Input::file('path')
				->move(public_path().'\img\writings',Input::file('path')->getClientOriginalName());
			$writing->path =  self::image_base_url.'/img/writings/'.Input::file('path')->getClientOriginalName();
		} else {
			$writing->path = "path";
		}

		// Null attributes.
		$writing->description = Input::get('description');
		$writing->title = Input::get('title');
		$writing->author = Input::get('author');
		$writing->published_at = Input::get('published_at');
		$writing->sender = Input::get('sender');
	}

	public function addIllustration($writing) {

		if ($writing) {
			$illustration = new Illustration;
			$illustration->writing = $writing->id;

			if (Input::hasFile('illustration_path')) {
				Input::file('illustration_path')
					->move(public_path().'\img\illustrations',Input::file('illustration_path')->getClientOriginalName());
				$illustration->path = public_path().'\img\illustrations'.Input::file('illustration_path')->getClientOriginalName();
			} else {
				$illustration->path = "path";
			}

			$illustration->save();

			if ($illustration) {
				$result = ['success'=>true, 'illustration'=>$illustration];
			} else {
				$result = ['success'=>false];
			}
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	public function anyRemoveIllustration() {
		$illustration = Illustration::find(Input::get('illustrationId'));

		if ($illustration) {
			$illustration->delete();
			$result = ['success'=>true];
		} else {
			$result = ['success'=>false];
		}

		return result;
	}

}