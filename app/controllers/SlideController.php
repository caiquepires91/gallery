<?php

class SlideController extends \BaseController {
	const PATH = '/img/slides';

	/**
	 * Display a listing of the resource.
	 * GET /slide
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return 'You are in Slides';
	}

	public function show($id) {
		$slide = Slide::find($id);
		$amount = Slide::count();
		return View::make('public.show',['slide' => $slide, 'amount'=>$amount, 'type' => 'slides']);
	}

	public function anyUpload() {
		$files = array();

		if (Input::hasFile('images')) {
			$files = Input::file('images');
			$rules = ['file' => 'required|image'];
			$destinationPath = public_path().self::PATH;

			foreach ($files as $file) {
				$validator = Validator::make(['file'=>$file], $rules);
				if ($validator->passes()) {
					$filename = $file->getClientOriginalName();
		            // move from /temp to your directory
					$upload_success = $file->move($destinationPath, $filename);
					if ($upload_success) {
						// generate a thumbnail
						$thumbnail = $this->createThumbnail($destinationPath, $filename);
						/* just for test puposes*/
						$done[] = $filename;
						Session::flash('done', $done);
						$uploaded[] = array(
						'name' => $filename,
						'file' => self::PATH . $filename
						);
					} else {
						$filename = $file->getClientOriginalName();
	                    $not[] = $filename;
	                    Session::flash('not', $not);
					}
				} else {
					return Redirect::back()->withErrors($validator);
				}
			}
		} else {
			return Redirect::back()->withErrors('choose a file');
		}

		return Response::json($uploaded);		
	}

	public function anyAdd(){
		$slide = new Slide;
		$this->setSlideAttributes($slide);
		$slide->save();

		if ($slide) {
			$result = ['success'=>true, 'slide'=>$slide];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	public function anyEdit() {
		$slide = Slide::find(Input::get('slideId'));

		if ($slide) {
			$this->setSlideAttributes($slide);
			$slide->save();
			$result = ['success'=>true, 'slide'=>$slide];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	public function anyRemove() {
		$slide = Slide::find(Input::get('slideId'));

		if ($slide) {
			$slide->delete();
			$result = ['success'=>true];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	/* 
	** Retreive 30 elements at a time
	** var amount = 30;
	** var index = 0, 30, 60, 70...
	*/
	public function anyAll() {
		$index = Input::get('index');
		$amount = 30;
		$start = $index - $amount;
		$slides = Slide::orderBy('updated_at','desc')->skip($start)->take($amount)->get();

		if ($slides) {
			$result = ['success'=>true, 'slides'=>$slides];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	public function anySearch() {
		$attribute = Input::get('attribute');
		$text = Input::get('text');
		$index = Input::get('index');
		$amount = 30;
		$start = $index - $amount;
		//$start = 0;
		$slides = Slide::where($attribute,'LIKE','%'.$text.'%')
			->orderBy('updated_at','desc')
			->skip($start)
			->take($amount)
			->get();

		if ($slides) {
			$result = ['success'=>true, 'slides'=>$slides];
		} else {
			$result = ['success'=>false];
		}

		return $result;
	}

	private function setSlideAttributes($slide) {
		// Not null attributes.
		$slide->code = Input::get('code');

		if (Input::hasFile('path')) {
			Input::file('path')
				->move(public_path().'/img/slides',Input::file('path')->getClientOriginalName());
			$slide->path =  self::image_base_url.'/img/slides/'.Input::file('path')->getClientOriginalName();
		} else {
			$slide->path = "path";
		}

		// Null attributes.
		$slide->description = Input::get('description');
		$slide->conservation = Input::get('conservation');
		$slide->geo_location = Input::get('geo_location');
		$slide->observation = Input::get('observation');
		$slide->brand = Input::get('brand');
		$slide->intervention_date = Input::get('intervention_date');
	}

}
