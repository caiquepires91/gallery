<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Order the routes from childs to parents.
// Session::get("current_id")
if (0) {
	// User is logged in.

	Route::controller('/user', 'UserController');
	Route::controller('/photo', 'PhotoController');
	Route::controller('/slide', 'SlideController');
	Route::controller('/negative', 'NegativeController');
	Route::controller('/writing', 'WritingController');

	Route::get('/logout', function(){
		Session::flush();
		return Redirect::guest("/")->with("You have logged out!"); //TODO: this message should reach login view
	});

	Route::get('/', function()
	{
		return View::make('hello')->with('message', "You're in!"); // Create a view for home page.
	});

} else {
	// User is NOT logged.

	// Since user is not logged, restritc his access to only these two methods.
	Route::get('/user.login', array('as' => 'user-login', 'uses' => 'UserController@AnyLogin'));
	Route::get('/user.register', array('as' => 'user-register', 'uses' => 'UserController@AnyRegister'));

	/* settings */
	Route::post('/settings.poem', ['as'=>'settings-poem', 'uses'=>'SettingsController@AnySavePoem']);
	Route::post('/settings.euvaldo', ['as'=>'settings-euvaldo', 'uses'=>'SettingsController@AnySaveEuvaldo']);
	Route::post('/settings.project', ['as'=>'settings-project', 'uses'=>'SettingsController@AnySaveProject']);
	Route::post('/settings.banner', ['as'=>'settings-banner', 'uses'=>'SettingsController@AnySaveBanner']);

	Route::controller('/settings', 'SettingsController');

	// Add media
	Route::post('/photo.add', ['as'=>'add-photo', 'uses'=>'PhotoController@AnyAdd']);
	Route::post('/negative.add', ['as'=>'add-negative', 'uses'=>'NegativeController@AnyAdd']);
	Route::post('/slide.add', ['as'=>'add-slide', 'uses'=>'SlideController@AnyAdd']);
	Route::post('/writing.add', ['as'=>'add-writing', 'uses'=>'WritingController@AnyAdd']);

	Route::post('/photo.upload', ['as' => 'photo-upload', 'uses' => 'PhotoController@AnyUpload']);
	Route::post('/negative.upload', ['as' => 'neg-upload', 'uses' => 'NegativeController@AnyUpload']);
	Route::post('/slide.upload', ['as' => 'slide-upload', 'uses' => 'SlideController@AnyUpload']);
	Route::post('/writing.upload', ['as' => 'writing-upload', 'uses' => 'WritingController@AnyUpload']);

	Route::post('/get.by.code', ['as' => 'get-by-code', 'uses' => 'PhotoController@getByCode']);
	Route::post('/media.edit', ['as' => 'media-edit', 'uses' => 'PhotoController@photoEdit']);

	/* Add media */
	Route::get('/add-photos', function() {
		return View::make('admin.draganddrop', ['type'=>'photos', 
			'photoamount'=>"", 'negativeamount'=>"", 'slideamount'=>"", 'writingamount'=>"",
			'pendingphotos'=>"", 'pendingslides'=>"", 'pendingwritings'=>"", 'pendingnegatives'=>""]);
	});
	Route::get('/add-negatives', function() {
		return View::make('admin.draganddrop', ['type'=>'negatives', 
			'photoamount'=>"", 'negativeamount'=>"", 'slideamount'=>"", 'writingamount'=>"",
			'pendingphotos'=>"", 'pendingslides'=>"", 'pendingwritings'=>"", 'pendingnegatives'=>""]);
	});
	Route::get('/add-slides', function() {
		return View::make('admin.draganddrop', ['type'=>'slides', 
			'photoamount'=>"", 'negativeamount'=>"", 'slideamount'=>"", 'writingamount'=>"",
			'pendingphotos'=>"", 'pendingslides'=>"", 'pendingwritings'=>"", 'pendingnegatives'=>""]);
	});
	Route::get('/add-writings', function() {
		return View::make('admin.draganddrop', ['type'=>'writings', 
			'photoamount'=>"", 'negativeamount'=>"", 'slideamount'=>"", 'writingamount'=>"",
			'pendingphotos'=>"", 'pendingslides'=>"", 'pendingwritings'=>"", 'pendingnegatives'=>""]);
	});

	/* List media */
	Route::get('/photos', function() {
		return View::make('admin.photos', ['type'=>'photos']);
	});
	Route::get('/negatives', function() {
		return View::make('admin.photos', ['type'=>'negatives']);
	});
	Route::get('/slides', function() {
		return View::make('admin.photos', ['type'=>'slides']);
	});
	Route::get('/writings', function() {
		return View::make('admin.photos', ['type'=>'writings']);
	});
	Route::get('/section/{section}', function($section) {
		return View::make('admin.photos', ['type'=>'sections', 'section'=>$section]);
	});

	Route::get('/edit/{type}', function($type) {
		return View::make('admin.edit', ['type'=>$type, 
			'photoamount'=>"", 'negativeamount'=>"", 'slideamount'=>"", 'writingamount'=>"",
			'pendingphotos'=>"", 'pendingslides'=>"", 'pendingwritings'=>"", 'pendingnegatives'=>""]);
	});

	Route::get('/dashboard',"PhotoController@dashboardAmount");

	/** visible to the public **/
	Route::post('/photo.get', ['as'=>'photo-get', 'uses'=>'PhotoController@AnyAll']);
	Route::post('/negative.get', ['as'=>'negative-get', 'uses'=>'NegativeController@AnyAll']);
	Route::post('/slide.get', ['as'=>'slide-get', 'uses'=>'SlideController@AnyAll']);
	Route::post('/writing.get', ['as'=>'writing-get', 'uses'=>'WritingController@AnyAll']);
	Route::post('/section.get', ['as'=>'section-get', 'uses'=>'WritingController@AnyAllWritings']);

	Route::post('/photo.search', ['as'=>'photo-search', 'uses'=>'PhotoController@AnySearch']);
	Route::post('/negative.search', ['as'=>'negative-search', 'uses'=>'NegativeController@AnySearch']);
	Route::post('/slide.search', ['as'=>'slide-search', 'uses'=>'SlideController@AnySearch']);
	Route::post('/writing.search', ['as'=>'writing-search', 'uses'=>'WritingController@AnySearch']);
	Route::post('/section.search', ['as'=>'section-search', 'uses'=>'WritingController@AnySearchWritings']);

	Route::get('/show-photos/{id}', function($id) {
		return View::make('/public.show',['type'=>'photos', 'id'=>$id]);
	});
	Route::get('/show-negatives/{id}', function($id) {
		return View::make('/public.show',['type'=>'negative', 'id'=>$id]);
	});
	Route::get('/show-slides/{id}', function($id) {
		return View::make('/public.show',['type'=>'slide', 'id'=>$id]);
	});
	Route::get('/show-writings/{id}', function($id) {
		return View::make('/public.show',['type'=>'writing', 'id'=>$id]);
	});

	Route::get('photo/{id}', 'PhotoController@show');
	Route::get('negative/{id}', 'NegativeController@show');
	Route::get('slide/{id}', 'SlideController@show');
	Route::get('writing/{id}', 'WritingController@show');
	//Route::get('section/{section}', 'WritingController@getWritingsBySection');

	Route::get('/gphotos', function() {
		return View::make('public.photos', ['type'=>'photos']);
	});
	Route::get('/gnegatives', function() {
		return View::make('public.photos', ['type'=>'negatives']);
	});
	Route::get('/gslides', function() {
		return View::make('public.photos', ['type'=>'slides']);
	});
	Route::get('/gwritings', function() {
		return View::make('public.photos', ['type'=>'writings']);
	});
	Route::get('/gsection/{section}', function($section) {
		//return ['type'=>'sections', 'section'=>$section];
		return View::make('public.photos', ['type'=>'sections', 'section'=>$section]);
	});
	Route::get('/gproject', 'SettingsController@showProject');
	Route::get('/gpoems', 'SettingsController@showPoem');
	Route::get('/gbiografy', 'SettingsController@showEuvaldo');

	Route::get('/', 'PhotoController@amount');
}