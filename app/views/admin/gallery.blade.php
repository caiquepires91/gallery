@extends('admin.dashboard')

<!-- Set back button -->
@section('logo', 'arrow_back')

<!-- Check here what button was hit and decide the titles-->
@if($type == 'photos')
	@section('title', 'Fotos')
	@section('navtitle', 'Fotos')
@elseif($type == 'negatives')
	@section('title', 'Negativos')
	@section('navtitle', 'Negativos')
@elseif($type == 'slides')
	@section('title', 'Slides')
	@section('navtitle', 'Slides')
@elseif($type == 'writings')
	@section('title', 'Escritos')
	@section('navtitle', 'Escritos')
@endif

@section('navbar')
    <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper black darken-2">
        <div class="my-logo">
          <a href="{{URL::to('/dashboard')}}" class="brand-logo"><i class='material-icons circle'>@yield('logo', 'photo_library')</i>@yield('navtitle', 'Acervo Euvaldo')</a>
        </div>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
          @section('navbar-menu')
          @show
        </ul>
      </div>
    </nav>
  	</div>
 @endsection

<!-- Get sidenav and leave it with no code to clear it from dashboard -->
@section('sidenav')
@endsection

<!-- Get body  and clear it -->
@section('body')

<header class="no-padding-left">
</header>

<main class="no-padding-left" style="background: black;">
	<div class="valign-wrapper" style="padding: 1%;">
	<div class="container">
		<div class="row valign-wrapper">
			<div class="col m1"><a href="#"><i class="material-icons">navigate_before</i></a></div>
			<div class="col m10">
				<div class="slider">
					<ul class="slides" style="height: 72vh;">
						<li>
						<img width="100%" src="{{URL::to('/img/photos/front/001.JPG')}}">
				        <div class="caption center-align" style="background-color:rgba(0, 0, 0, 0.3);">
				          <h3>This is our big Tagline!</h3>
				          <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
				        </div>
					    </li>
					</ul>
				</div>
			</div>
			<div class="col m1 right-align"><a  href="#"><i class="material-icons">navigate_next</i></a></div>
		</div>
		<h5 class="grey-text center-align">1/100</h4>
		<div class="row">
			<div class="col m12">
				<div class="carousel">
			    <a class="carousel-item" href="#one!"><img src="{{URL::to('/img/photos/front/thumbnails/001.JPG')}}"></a>
			    <a class="carousel-item" href="#two!"><img src="{{URL::to('/img/photos/front/thumbnails/002.JPG')}}"></a>
			    <a class="carousel-item" href="#three!"><img src="{{URL::to('/img/photos/front/thumbnails/003.JPG')}}"></a>
			    <a class="carousel-item" href="#four!"><img src="{{URL::to('/img/photos/front/thumbnails/004.JPG')}}"></a>
			    <a class="carousel-item" href="#five!"><img src="{{URL::to('/img/photos/front/thumbnails/005.JPG')}}"></a>
			    <a class="carousel-item" href="#six!"><img src="{{URL::to('/img/photos/front/thumbnails/006.JPG')}}"></a>
			    <a class="carousel-item" href="#seven!"><img src="{{URL::to('/img/photos/front/thumbnails/007.JPG')}}"></a>
			    <a class="carousel-item" href="#eight!"><img src="{{URL::to('/img/photos/front/thumbnails/008.JPG')}}"></a>
			    <a class="carousel-item" href="#nine!"><img src="{{URL::to('/img/photos/front/thumbnails/006.JPG')}}"></a>
			    <a class="carousel-item" href="#ten!"><img src="{{URL::to('/img/photos/front/thumbnails/007.JPG')}}"></a>
			    <a class="carousel-item" href="#eleven!"><img src="{{URL::to('/img/photos/front/thumbnails/008.JPG')}}"></a>
			    <a class="carousel-item" href="#twelve!"><img src="{{URL::to('/img/photos/front/thumbnails/001.JPG')}}"></a>
			    <a class="carousel-item" href="#thirteen!"><img src="{{URL::to('/img/photos/front/thumbnails/002.JPG')}}"></a>
			    <a class="carousel-item" href="#fourteen!"><img src="{{URL::to('/img/photos/front/thumbnails/003.JPG')}}"></a>
			    <a class="carousel-item" href="#fifteen!"><img src="{{URL::to('/img/photos/front/thumbnails/004.JPG')}}"></a>
			    <a class="carousel-item" href="#sixteen!"><img src="{{URL::to('/img/photos/front/thumbnails/005.JPG')}}"></a>
			    <a class="carousel-item" href="#seventeen!"><img src="{{URL::to('/img/photos/front/thumbnails/006.JPG')}}"></a>
			    <a class="carousel-item" href="#eighteen!"><img src="{{URL::to('/img/photos/front/thumbnails/001.JPG')}}"></a>
			    <a class="carousel-item" href="#ninteen!"><img src="{{URL::to('/img/photos/front/thumbnails/002.JPG')}}"></a>
			    <a class="carousel-item" href="#twenty!"><img src="{{URL::to('/img/photos/front/thumbnails/003.JPG')}}"></a>
			    <a class="carousel-item" href="#twenty-one!"><img src="{{URL::to('/img/photos/front/thumbnails/004.JPG')}}"></a>
			    <a class="carousel-item" href="#twenty-two!"><img src="{{URL::to('/img/photos/front/thumbnails/005.JPG')}}"></a>
			    <a class="carousel-item" href="#twenty-three!"><img src="{{URL::to('/img/photos/front/thumbnails/006.JPG')}}"></a>
			  </div>
			</div>
		</div>
	</div>
	</div>
</main>

<footer class="no-padding-left">
</footer>

@endsection