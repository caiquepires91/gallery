@extends('admin.dashboard')

@section('head')
 <link type="text/css" rel="stylesheet" href="css/draganddrop.css"  media="screen,projection"/>
@endsection

<!-- Set back button -->
@section('logo', 'arrow_back')

<!-- Check here what button was hit and decide the titles-->
@if($type == 'photos')
	@section('title', 'Adicionar Fotos')
	@section('navtitle', 'Adicionar Fotos')
	{{Session::put('action','photo.upload')}}
@elseif($type == 'negatives')
	@section('title', 'Adicionar Negativos')
	@section('navtitle', 'Adicionar Negativos')
	Session::put('action','negative.upload');
@elseif($type == 'slides')
	@section('title', 'Adicionar Slides')
	@section('navtitle', 'Adicionar Slides
	Session::put('action','slide.upload');
@elseif($type == 'writings')
	@section('title', 'Adicionar Escritos')
	@section('navtitle', 'Adicionar Escritos')
	Session::put('action','writing.upload');
@endif

<!-- Get sidenav and leave it with no code to clear it from dashboard -->
@section('sidenav')
@endsection

<!-- Get body  and clear it -->
@section('body')

<!-- Header -->
<header class="no-padding-left">
</header>

<!-- Main -->
<main class="no-padding-left valign-wrapper">

	<div class="container">

		<div class="row card">
			<div class="col m12">
				<div class="dropzone" id="dropzone" data-type="{{Session::get('action')}}" type="{{$type}}" data-url="{{URL::to('/')}}" style="background-color:rgba(255, 255, 255, 0.8);">
					<div id="media-row" class="row">
						<!-- start column -->
						<!-- end column -->	
					</div>
					<div class="insidedrop">
						<i class="material-icons">add_photo_alternate</i>
						<p class="">Arraste e solte até 30 imagens .jpeg (max.: 2MB cada) para fazer upload.</p>
					</div>
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m6 l6">
				<div style="color: #585353">
					<span id="progress"></span>
					<span class="middle-dot"></span>
					<span id="result"></span>
				</div>
			</div>
			<div class="col s12 m6 l6 right-align">
				<a id="btn-cancel"  class="btn-flat disabled waves-effect waves-light btn white-text">Cancelar<i class="material-icons right">cancel</i></a>
				<a id="btn-save" class="btn-flat disabled waves-effect waves-light btn white-text">Salvar<i class="material-icons right">cloud_upload</i></a>
			</div>
		</div>
	</div>	
</main>

<!-- Footer -->
<footer class="no-padding-left">
	
</footer>

@section('script')
   <script type="text/javascript" src="js/draganddrop.js"></script>
@endsection

@endsection