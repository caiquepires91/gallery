<!DOCTYPE html>
<html>
<head>
	<!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('css/materialize.min.css') }}"  media="screen,projection"/>
    <!--Import style.css-->
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('css/public-gallery.css') }}"  media="screen,projection"/>
     <!-- import some design icons -->
    <link href="{{ URL::asset('css/materialdesignicons.min.css') }}" media="all" rel="stylesheet" type="text/css"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Acervo Euvaldo - @yield('title', 'Imagens')</title>

    <!-- Set back button -->
	@section('logo', 'arrow_back')

    <!-- Check here what button was hit and decide the titles-->
	@if($type == 'photos')
		@section('title', 'Fotos')
		@section('navtitle', 'Fotos')
	@elseif($type == 'negatives')
		@section('title', 'Negativos')
		@section('navtitle', 'Negativos')
	@elseif($type == 'slides')
		@section('title', 'Diapositos (slides)')
		@section('navtitle', 'Diapositos (slides)')
	@elseif($type == 'writings')
		@section('title', 'Escritos')
		@section('navtitle', 'Escritos')
	@elseif($type == 'sections')
		@section('title', $section)
		@section('navtitle', $section)
	@endif
</head>
<body>
	<header>
		<nav>
		    <div class="nav-wrapper">
		    	<div class="left">
		    		<a href="{{URL::to('/dashboard')}}" class="brand-logo"><i class='material-icons back-button'>@yield('logo')</i>@yield('navtitle', 'Fotos')</a>
		    	</div>
		      	<ul class="right center valign-wrapper">
			        <li class="search-bar hide-on-med-to-down">
			        	<form>
				        	<div class="input-field" style="height: fit-content;">
					          <i class="material-icons prefix">search</i>
					          <input class="white-text" id="icon_prefix" type="search" style="margin: 0; background-color: rgba(255, 255, 255, 0.5);">
					          <label for="icon_prefix">Buscar</label>
					        </div>
				        </form>
		      		</li>
			        <li><a id="filter" href="#"><i class="white-text mdi mdi-filter mdi-24px"></i></a></li>
			        <li><a class="reload" href="#"><i class="material-icons">refresh</i></a></li>
	     		</ul>
	     	</div>
		    </div>
  		</nav>
  		<div class="second-bar valign-wrapper">
			<a class="valign-wrapper white-text" href="{{URL::to('/dashboard')}}" class="">
				<i class='material-icons back-button'>@yield('logo')</i>
				<span class="second-logo">@yield('navtitle', 'Fotos')</span>
			</a>
		</div>
  		<div class="board">
  			<div class="container">
  				<div class="row">
	  				<div class="col m4 valign-wrapper">
	  					<div class="switch">
						    <label>
						      <input id="filter-title" type="checkbox" checked="checked">
						      <span class="lever"></span>
						    </label>
	  					</div>
	  					<p id="filter-label-title" class="filter-label">Título<p>
	  				</div>
	  				<div class="col m4 valign-wrapper">
	  					<div class="switch">
						    <label>
						      <input id="filter-code" type="checkbox">
						      <span class="lever"></span>
						    </label>
	  					</div>
	  					<p id="filter-label-code" class="filter-label">Código<p>
	  				</div>
	  				<div class="col m4 valign-wrapper">
	  					<div class="switch">
						    <label>
						      <input id="filter-label" type="checkbox">
						      <span class="lever"></span>
						    </label>
	  					</div>
	  					<p id="filter-label-label" class="filter-label">Legenda<p>
	  				</div>
  				</div>
  			</div>
  		</div>
	</header>

	<main class="main">
		<div class="container">
			@if (isset($section))
			<div type={{$type}}	section="{{$section}}" class="row photo-content">
			</div>
			@else
			<div type={{$type}} class="row photo-content">
			</div>
			@endif
		</div>
	</main>

	<footer>
	</footer>

	 <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="{{ URL::asset('js/materialize.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/admin-gallery.js') }}"></script>
</body>
</html>