<!DOCTYPE html>
<html>
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('css/materialize.min.css') }}"  media="screen,projection"/>
    <!--Import style.css-->
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('css/style.css') }}"  media="screen,projection"/>

    @section('head')
    @show

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Acervo Euvaldo - @yield('title', 'Dashboard')</title>
  </head>

  <body>

    @section('navbar')
    <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper" style="background-color: #001165c2">
        <div class="my-logo">
          <a href="@yield('link', URL::to('/dashboard'))" class="brand-logo"><i class='material-icons circle'>@yield('logo', 'photo_library')</i>@yield('navtitle', 'Acervo Euvaldo')</a>
        </div>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
          @section('navbar-menu')
          @show
        </ul>
      </div>
    </nav>
  </div>
    @show

    @section('sidenav')
     <ul id="slide-out" class="side-nav fixed">
      <li>
        <div class="user-view">
          <div class="background">
            <img src="img/helper/sidenav-background.jpg">
          </div>
          <a href="#!user"><img class="circle" src="img/helper/sidenav-face.jpg"></a>
          <a href="#!name"><span class="white-text name">Maria João</span></a>
          <a href="#!email"><span class="white-text email">mariajoão@gmail.com</span></a>
        </div>
      </li>
      <li><a href="{{URL::to('/photos')}}"><i class='material-icons circle'>photo</i>Fotos</a></li>
      <li><a href="{{URL::to('/negatives')}}"><i class='material-icons circle'>camera_roll</i>Negativos</a></li>
      <li><a href="{{URL::to('/slides')}}"><i class='material-icons circle'>slideshow</i>Slides</a></li>
      <li><a href="{{URL::to('/writings')}}"><i class='material-icons circle'>insert_drive_file</i>Escritos</a></li>
    </ul>

    <ul class="left hide-on-med-and-down">
      <li><a href="{{URL::to('/photos')}}">Fotos</a></li>
      <li><a href="{{URL::to('/negatives')}}">Negativos</a></li>
      <li><a href="{{URL::to('/slides')}}">Slides</a></li>
      <li><a href="{{URL::to('/writings')}}">Escritos</a></li>
    </ul>

    <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
    @show

    @section('body')

    <header>
      
    </header>

    <main>
      <div class="container">
        <div class="row">

          <div class="col m5 offset-m1">
            <div class="card">
              <div class="card-image">
                <img src="img/helper/dashboard/photos-2.jpg">
                <span class="card-title">Fotos</span>
                <a class="btn-floating halfway-fab waves-effect waves-light" href="{{URL::to('/add-photos')}}"><i class="material-icons">add</i></a>
              </div>
              <div class="card-content" style="padding: 15px;">
                <h4 class="thin" style="margin: 0px;">{{$photoamount}} fotos</h4>
                <p class="purple-text text-darken-2">{{$pendingphotos}} pendentes</p>
              </div>
            </div>
          </div>

          <div class="col m5">
            <div class="card">
              <div class="card-image">
                <img src="img/helper/dashboard/negatives.jpg">
                <span class="card-title">Negativos</span>
                <a class="btn-floating halfway-fab waves-effect waves-light" href="{{URL::to('/add-negatives')}}"><i class="material-icons">add</i></a>
              </div>
              <div class="card-content" style="padding: 15px;">
                <h4 class="thin" style="margin: 0px;">{{$negativeamount}} negativos</h4>
                <p class="purple-text text-darken-2">{{$pendingnegatives}} pendentes</p>
              </div>
            </div>
          </div>

          <div class="col m5 offset-m1">
            <div class="card">
              <div class="card-image">
                <img src="img/helper/dashboard/slides-2.jpg">
                <span class="card-title">Slides</span>
                <a class="btn-floating halfway-fab waves-effect waves-light" href="{{URL::to('/add-slides')}}"><i class="material-icons">add</i></a>
              </div>
              <div class="card-content" style="padding: 15px;">
               <h4 class="thin" style="margin: 0px;">{{$slideamount}} slides</h4>
                <p class="purple-text text-darken-2">{{$pendingslides}} pendentes</p>
              </div>
            </div>
          </div>

          <div class="col m5">
            <div class="card">
              <div class="card-image">
                <img src="img/helper/dashboard/writings.jpg">
                <span class="card-title">Escritos</span>
                <a class="btn-floating halfway-fab waves-effect waves-light" href="{{URL::to('/add-writings')}}"><i class="material-icons">add</i></a>
              </div>
              <div class="card-content" style="padding: 15px;">
                <h4 class="thin" style="margin: 0px;">{{$writingamount}} escritos</h4>
                <p class="purple-text text-darken-2">{{$pendingwritings}} pendentes</p>
              </div>
            </div>
          </div>

          <div class="col m5 offset-m1">
            <div class="card">
              <div class="card-image">
                <img src="img/helper/dashboard/settings.jpg">
                <span class="card-title">Configurações</span>
                <a class="btn-floating halfway-fab waves-effect waves-light" href="{{URL::to('/settings')}}"><i class="material-icons">settings</i></a>
              </div>
              <div class="card-content" style="padding: 15px;">
                <p class="purple-text text-darken-2">Banner, Sobre o Projeto, Poesias...</p>
              </div>
            </div>
          </div>
        </div> 
      </div>
    </main>

    <footer>
      
    </footer>

    @show

    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="{{ URL::asset('js/materialize.min.js') }}"></script>
    <script src="{{ URL::asset('js/gallery.js') }}"></script>
    @section('script')
    @show
  </body>
</html>