@extends('admin.dashboard')

@section('head')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
	<link type="text/css" rel="stylesheet" href="{{URL::asset('css/edit.css')}}"  media="screen,projection"/>
@endsection

<!-- Set back button -->
@section('logo', 'arrow_back')
@section('link', URL::to('/add-photos'))

<!-- Check here what button was hit and decide the titles-->
@if($type == 'photos')
	@section('title', 'Editar Fotos')
	@section('navtitle', 'Editar Fotos')
	{{Session::put('action','photo.upload')}}
@elseif($type == 'negatives')
	@section('title', 'Editar Negativos')
	@section('navtitle', 'Editar Negativos')
	Session::put('action','negative.upload');
@elseif($type == 'slides')
	@section('title', 'Editar Slides')
	@section('navtitle', 'Editar Slides
	Session::put('action','slide.upload');
@elseif($type == 'writings')
	@section('title', 'Editar Escritos')
	@section('navtitle', 'Editar Escritos')
	Session::put('action','writing.upload');
@endif

<!-- Get sidenav and leave it with no code to clear it from dashboard -->
@section('sidenav')
@endsection

<!-- Get body  and clear it -->
@section('body')

<!-- Header -->
<header class="no-padding-left">
</header>

<!-- Main -->
<main class="no-padding-left valign-wrapper">

	<div class="container">

		<div class="row card panel" data-url="{{URL::to('/')}}">
			<div class="col m12">
				<div data-type="{{Session::get('action')}}" style="background-color:rgba(255, 255, 255, 0.8);">
					<div class="row">
						<div class="col m5">
							<a class="btn-prev nav-button waves-effect waves-light btn disabled"><i class="material-icons">arrow_left</i></a><a class="btn-next nav-button waves-effect waves-light btn disabled"><i class="material-icons">arrow_right</i></a>
						</div>
						<div class="col m2 center counter" style="color: #9e9e9e;">
							1/1
						</div>
						{{ Form::open(['method'=>'post','url'=>'media.edit', 'id'=>'media-form'])}}
						<div class="col m5 right-align">
							<input type="checkbox" name="pending" class="filled-in" id="checkbox1"/>
          		<label for="checkbox1">Pendente</label>
				    </div>
					</div>
					<div class="divider"></div>
					<div class="row">
						<div class="media col m6 s12 black">
							<div class="media-placeholder valign-wrapper">
								<img class="materialboxed prop" src="{{URL::asset('img/helper/banner/b1.jpg')}}">
							</div>
						</div>
							<div class="input-field col m6 s12">
					      <i class="material-icons prefix">image</i>
					      {{ Form::text('code', null, ['id'=>'code-input']) }}
					      {{ Form::label('code', 'Código')}}
					    </div>
					    <div class="input-field col m6 s12">
					      <i class="material-icons prefix">photo_library</i>
					      {{ Form::text('series', null, ['id'=>'series-input']) }}
					      {{ Form::label('series', 'Série')}}
					    </div>
					    <div class="input-field col m6 s12">
					      <i class="material-icons prefix">invert_colors</i>
					      {{ Form::text('color', null, ['id'=>'color-input']) }}
					      {{ Form::label('color', 'Cor')}}
					    </div>
					     <div class="input-field col m3 s12">
					      <i class="material-icons prefix">crop_landscape</i>
					      {{ Form::number('width', null, ['id'=>'width-input', "step"=>0.01]) }}
					      {{ Form::label('width', 'Largura')}}
					    </div>
					    <div class="input-field col m3 s12">
					      <i class="material-icons prefix">crop_portrait</i>
					      {{ Form::number('height', null, ['id'=>'height-input', 'step'=>0.01]) }}
					      {{ Form::label('height', 'Altura')}}
					    </div>
					    <div class="input-field col s12">
				          {{ Form::textarea('label', null, ['id'=>'label-input', 'class'=>'materialize-textarea', "data-length"=>"200"])}}
				          {{ Form::label('label', 'Legenda')}}
				      </div>
					    <div class="input-field col s12">
				          {{ Form::textarea('conservation', null, ['id'=>'conservation-input', 'class'=>'materialize-textarea', "data-length"=>"200"])}}
				          {{ Form::label('conservation', 'Danos')}}
				        </div>
				        <div class="input-field col m6 s12">
					      <i class="material-icons prefix">location_city</i>
					      {{ Form::text('geo_location', null, ['id'=>'location-input']) }}
					      {{ Form::label('geo_location', 'Localização Geográfica')}}
					    </div>
					    <div class="input-field col m6 s12">
					      <i class="material-icons prefix">brush</i>
					      {{ Form::text('executed_treatment', null, ['id'=>'executed-treatment-input']) }}
					      {{ Form::label('executed_treatment', 'Tratamento Executado')}}
					    </div>
					    <div class="input-field col s12">
				          {{ Form::textarea('back_note', null, ['id'=>'back-notes-input', 'class'=>'materialize-textarea', "data-length"=>"200"])}}
				          {{ Form::label('back_note', 'Anotações Frontais')}}
				        </div>
				        <div class="input-field col s12">
				          {{ Form::textarea('front_note', null, ['id'=>'front-notes-input', 'class'=>'materialize-textarea', "data-length"=>"200"])}}
				          {{ Form::label('front_note', 'Anotações Traseiras')}}
				        </div>
				        <div class="input-field col m6 s12">
					      <i class="material-icons prefix">today</i>
					      {{ Form::text('intervention', null, ['id'=>'intervention-input', "class"=>"datepicker"]) }}
					      {{ Form::label('intervention', 'Data de Intervenção')}}
					    </div>
					    <div class="input-field col m6 s12">
					      <i class="material-icons prefix">aspect_ratio</i>
					      {{ Form::text('framing', null, ['id'=>'framing-input']) }}
					      {{ Form::label('framing', 'Enquadramento')}}
					    </div>
					</div>
					<div class="divider"></div>
					<div class="row">
						<div class="col m5">
							<a class="btn-prev nav-button waves-effect waves-light btn disabled"><i class="material-icons">arrow_left</i></a><a class="btn-next nav-button waves-effect waves-light btn disabled"><i class="material-icons">arrow_right</i></a>
						</div>
						<div class="col m2 center counter" style="color: #9e9e9e;">
							1/1
						</div>
					</div>					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m6 l6">
				<a id="btn-done" href="{{URL::to('/dashboard')}}" class="btn-flat waves-effect waves-light btn grey-text text-darken-2">Concluir<i class="material-icons right">cloud_done</i></a>
			</div>
			<div class="col s12 m6 l6 right-align">
				<a id="btn-save" class="btn-flat waves-effect waves-light btn grey-text text-darken-2" type="submit" name="action">Salvar<i class="material-icons right">save</i></a>
			</div>
		</div>
		{{ Form::close() }}
	</div>

</main>

<!-- Footer -->
<footer class="no-padding-left">
	
</footer>

@section('script')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
	<script type="text/javascript" src="{{URL::asset('js/edit.js')}}"></script>
@endsection

@endsection