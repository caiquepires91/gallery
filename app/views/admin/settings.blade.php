<!DOCTYPE html>
<html>
<head>
	<!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <!--Import style.css-->
    <link type="text/css" rel="stylesheet" href="css/settings.css"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Acervo Euvaldo - @yield('title', 'Configurações')</title>

    <!-- Set back button -->
	@section('logo', 'arrow_back')

    <!-- Check here what button was hit and decide the titles-->
	@section('title', 'Configurações')
	@section('navtitle', 'Configurações')
</head>
<body>
	<header>
		<nav>
		    <div class="nav-wrapper">
		    	<div class="left">
		    		<a href="{{URL::to('/dashboard')}}" class="brand-logo" style="padding-left: 22px;"><i class='material-icons back-button'>@yield('logo')</i>@yield('navtitle', 'Configurações')</a>
		    	</div>
	     	</div>
  		</nav>
	</header>

	<main>
		<div class="container">
		<div class="row">
			<div id="settings-data" data-project='{{urlencode($project)}}' data-group='{{urlencode($group)}}' data-euvaldo='{{urlencode($euvaldo)}}' data-poem='{{urlencode($poem)}}' data-banner-1={{urlencode($banner1)}} data-banner-2={{urlencode($banner2)}} data-banner-3={{urlencode($banner3)}} style="display: none;"></div>
			<div class="col s12 m12">
				<h5>Gerais</h5>
			   	<span>Edite as informações das páginas de poesia, sobre o projeto, sobre o acervo, e outras partes da galeria.</span>
		  	</div>
			<div class="col s12 m12">
			    <ul class="collapsible" data-collapsible="accordion">
			    	 <li>
				      <div class="collapsible-header"><i class="material-icons">burst_mode</i>Banner</div>
				      <div class="collapsible-body">
			      		<div class="row">
				      		<div class="col s12">
				      			<span>Para editar as informações, modifique os campos e clique em <code class=" language-markup">"salvar"</code>.</span>
				      		</div>
				      	</div>
				      	{{ Form::open(['method'=>'post', 'url'=>'settings.banner', 'id'=>'settings-banner-form', 'files'=>true]) }}
				      	<div class="section">
						 	<h5>Banner 1</h5>
						    <div class="row">
						        <div class="input-field col s12">
						        	{{ Form::text('banner-title-1', null, ['class'=>'materialize-textarea', 'id'=>'banner-title-1'])}}
	          						{{ Form::label('banner-title-1', 'Título')}}
						        </div>
						        <div class="input-field col m6">
						        	{{ Form::text('banner-subtitle-1', null, ['class'=>'materialize-textarea', 'id'=>'banner-subtitle-1'])}}
	          						{{ Form::label('banner-subtitle-1', 'Subtítulo')}}
						        </div>
						        <div class="input-field col m6">
						        	{{ Form::text('banner-link-1', null, ['class'=>'materialize-textarea', 'id'=>'banner-link-1'])}}
	          						{{ Form::label('banner-link-1', 'Link')}}
						        </div>
						        <div class="input-field col s12">
						        	 <div class="file-field input-field">
								      <div class="btn">
								        <span>File</span>
								        {{ Form::file('banner_file_1', ['enctype'=>'multipart/form-data', 'accept'=>'.jpg']) }}
								      </div>
								      <div class="file-path-wrapper">
								      	{{ Form::text('banner-file-path-1', null, ['class'=>'file-path validate materialize-textarea', 'placeholder'=>"Apenas arquivos .jpg"]) }}
								      </div>
								    </div>
						        </div>
						    </div>
						</div>
						<div class="section">
						 	<h5>Banner 2</h5>
						    <div class="row">
						        <div class="input-field col s12">
						        	{{ Form::text('banner-title-2', null, ['class'=>'materialize-textarea', 'id'=>'banner-title-2'])}}
	          						{{ Form::label('banner-title-2', 'Título')}}
						        </div>
						        <div class="input-field col m6">
						        	{{ Form::text('banner-subtitle-2', null, ['class'=>'materialize-textarea', 'id'=>'banner-subtitle-2'])}}
	          						{{ Form::label('banner-subtitle-2', 'Subtítulo')}}
						        </div>
						        <div class="input-field col m6">
						        	{{ Form::text('banner-link-2', null, ['class'=>'materialize-textarea', 'id'=>'banner-link-2'])}}
	          						{{ Form::label('banner-link-2', 'Link')}}
						        </div>
						        <div class="input-field col s12">
						        	 <div class="file-field input-field">
								      <div class="btn">
								        <span>File</span>
								        {{ Form::file('banner_file_2', ['class' => 'field', 'enctype'=>'multipart/form-data', 'accept'=>'.jpg']) }}
								      </div>
								      <div class="file-path-wrapper">
								      	{{ Form::text('banner-file-path-2', null, ['class'=>'materialize-textarea file-path validate', 'placeholder'=>"Apenas arquivos .jpg"]) }}
								      </div>
								    </div>
						        </div>
						    </div>
						</div>
						<div class="section">
						 	<h5>Banner 3</h5>
						    <div class="row">
						        <div class="input-field col s12">
						        	{{ Form::text('banner-title-3', null, ['class'=>'materialize-textarea', 'id'=>'banner-title-3'])}}
	          						{{ Form::label('banner-title-3', 'Título')}}
						        </div>
						        <div class="input-field col m6">
						        	{{ Form::text('banner-subtitle-3', null, ['class'=>'materialize-textarea', 'id'=>'banner-subtitle-3'])}}
	          						{{ Form::label('banner-subtitle-3', 'Subtítulo')}}
						        </div>
						        <div class="input-field col m6">
						        	{{ Form::text('banner-link-3', null, ['class'=>'materialize-textarea', 'id'=>'banner-link-3'])}}
	          						{{ Form::label('banner-link-3', 'Link')}}
						        </div>
						         <div class="input-field col s12">
						        	 <div class="file-field input-field">
								      <div class="btn">
								        <span>File</span>
								        {{ Form::file('banner_file_3', ['class' => 'field', 'enctype'=>'multipart/form-data', 'accept'=>'.jpg']) }}
								      </div>
								      <div class="file-path-wrapper">
								      	{{ Form::text('banner-file-path-3', null, ['class'=>'materialize-textarea file-path validate', 'placeholder'=>"Apenas arquivos .jpg"]) }}
								      </div>
								    </div>
						        </div>
						    </div>
						</div>
						 <div>
				        	<button id="save-banner-btn" class="waves-effect waves-light settings-btn btn-flat" type="submit" name="action">Salvar
						        <i class="material-icons left">save</i>
						    </button>
				        </div>
						{{ Form::close() }}
				      </div>
				    </li>
				    <li>
				      <div class="collapsible-header"><i class="material-icons">work</i>Sobre o Projeto</div>
				      <div class="collapsible-body">
			      		<div class="row">
				      		<div class="col s12">
				      			<span>Para editar as informações, modifique os campos e clique em <code class=" language-markup">"salvar"</code>.</span>
				      		</div>
				      	</div>
				      	{{ Form::open(['method'=>'post', 'url'=>'settings.project', 'id'=>'settings-project-form']) }}
				      	 <div class="section">
						 	<h5>Sobre o Projeto</h5>
						      <div class="row">
						        <div class="input-field col s12">
						         	{{ Form::textarea('project-introduction', null, ['class'=>'materialize-textarea', "data-length"=>"650", 'id'=>'project-introduction'])}}
	          						{{ Form::label('project-introduction', 'Introdução')}}
						        </div>
						        <div class="input-field col s12">
						        	{{ Form::textarea('project-text', null, ['class'=>'materialize-textarea', 'id'=>'project-text'])}}
	          						{{ Form::label('project-text', 'Texto')}}
						        </div>
						      </div>
						  </div>
						  <div class="section">
						    <h5>Sobre a Equipe</h5>
						      <div class="row">
						        <div class="input-field col s12">
						         	{{ Form::textarea('group-introduction', null, ['class'=>'materialize-textarea', "data-length"=>"650", 'id'=>'group-introduction'])}}
	          						{{ Form::label('group-introduction', 'Introdução')}}
						        </div>
						        <div class="input-field col s12">
						        	{{ Form::textarea('group-text', null, ['class'=>'materialize-textarea', 'id'=>'group-text'])}}
	          						{{ Form::label('group-text', 'Texto')}}
						        </div>
						      </div>
						       <div>
						        	<button id="save-project-btn" class="waves-effect waves-light settings-btn btn-flat" type="submit" name="action">Salvar
								        <i class="material-icons left">save</i>
								    </button>
						        </div>
						    {{ Form::close() }}
						  </div>
				      </div>
				    </li>
				    <li>
				      <div class="collapsible-header"><i class="material-icons">portrait</i>Sobre Euvaldo</div>
				      <div class="collapsible-body">
				      	<div class="row">
				      		<div class="col s12">
				      			<span>Para editar as informações, modifique os campos e clique em <code class=" language-markup">"salvar"</code>.</span>
				      		</div>
				      	</div>
					    {{ Form::open(['method'=>'post', 'url'=>'settings.euvaldo', 'id'=>'settings-euvaldo-form']) }}
					      <div class="row">
					        <div class="input-field col s12">
					         	{{ Form::textarea('introduction', null, ['class'=>'materialize-textarea', "data-length"=>"650", 'id'=>'euvaldo-introduction'])}}
          						{{ Form::label('introduction', 'Introdução')}}
					        </div>
					        <div class="input-field col s12">
					        	{{ Form::textarea('text', null, ['class'=>'materialize-textarea', 'id'=>'euvaldo-text'])}}
          						{{ Form::label('text', 'Texto')}}
					        </div>
					      </div>
					       <div>
					        	<button id="save-euvaldo-btn" class="waves-effect waves-light settings-btn btn-flat" type="submit" name="action">Salvar
							        <i class="material-icons left">save</i>
							    </button>
					        </div>
					    {{ Form::close() }}
				      </div>
				    </li>
				    <li>
				      <div class="collapsible-header active"><i class="material-icons">local_florist</i>Poesias</div>
				      <div class="collapsible-body">
				      	<div class="row">
				      		<div class="col s12">
				      			<span>Para editar as informações, modifique os campos e clique em <code class=" language-markup">"salvar"</code>.</span>
				      		</div>
				      	</div>
					    {{ Form::open(['method'=>'post', 'url'=>'settings.poem', 'id'=>'settings-poem-form']) }}
					      <div class="row">
					      	<div class="input-field col s12">
					         	{{ Form::textarea('quotation', null, ['class'=>'materialize-textarea', "data-length"=>"650", 'placeholder'=>"O texto inserido aqui ficará exposto em um card na página inicial", 'id'=>'poem-quotation'])}}
          						{{ Form::label('quotation', 'Citação')}}
					        </div>
					        <div class="input-field col s12 m5 offset-m7">
					         	{{ Form::text('quotation-author', null, ['class'=>'materialize-textarea', "data-length"=>"45", 'id'=>'poem-quotation-author'])}}
          						{{ Form::label('quotation-author', 'Autor da Citação')}}
					        </div>
					        <div class="input-field col s12">
					         	{{ Form::textarea('introduction', null, ['class'=>'materialize-textarea', "data-length"=>"650", 'id'=>'poem-introduction'])}}
          						{{ Form::label('introduction', 'Introdução')}}
					        </div>
					        <div class="input-field col s12 m6">
					         	{{ Form::text('title', null, ["data-length"=>"50", 'id'=>'poem-title'])}}
          						{{ Form::label('title', 'Titulo')}}
					        </div>
					        <div class="input-field col s12 m6">
					        	{{ Form::text('author', null, ['id'=>'poem-author']) }}
  								{{ Form::label('author', 'Autor')}}
					        </div>
					        <div class="input-field col s12">
					        	{{ Form::textarea('text', null, ['class'=>'materialize-textarea', 'id'=>'poem-text'])}}
          						{{ Form::label('text', 'Texto')}}
					        </div>
					      </div>
					       <div>
					        	<button id="save-poem-btn" class="waves-effect waves-light settings-btn btn-flat" type="submit" name="action">Salvar
							        <i class="material-icons left">save</i>
							    </button>
					        </div>
					    {{ Form::close() }}
				      </div>
				    </li>
				  </ul>
		  	</div>
		</div>
		</div>
	</main>

	<footer>
	</footer>

	 <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/settings.js"></script>
</body>
</html>