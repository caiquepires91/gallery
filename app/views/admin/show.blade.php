@extends('public.gallery-layout')

@section('head')
	 <!--Import style.css-->
    <link type="text/css" rel="stylesheet" href="{{URL::to('css/show.css')}}"  media="screen,projection"/>
@endsection

<!-- Set back button -->
@section('logo', 'arrow_back')

<!-- Check here what button was hit and decide the titles-->
@if($type == 'photos')
	@section('title', 'Fotos')
	@section('navtitle', 'Fotos')
	@section('backurl', URL::to('/gphotos'))
	{{Session::put('title', $photo->title)}}
	{{Session::put('tag_1', "LEGENDA")}}
	{{Session::put('description', $photo->label)}}
	{{Session::put('code', $photo->code)}}
	{{Session::put('tag_2', "PUBLICADO EM")}}
	{{Session::put('published_at', $photo->published_at)}}
	{{Session::put('geo_location', $photo->geo_location)}}
	{{Session::put('series', $photo->series)}}
	{{Session::put('support', $photo->support)}}
	{{Session::put('media', $photo)}}
@elseif($type == 'negatives')
	@section('title', 'Negativos')
	@section('navtitle', 'Negativos')
	@section('backurl', URL::to('/gnegatives'))
	{{Session::put('title', $negative->collection)}}
	{{Session::put('tag_1', "DESCRIÇÃO")}}
	{{Session::put('description', $negative->description)}}
	{{Session::put('code', $negative->code)}}
	{{Session::put('tag_2', "DATA DE INTERVENÇÃO")}}
	{{Session::put('published_at', $negative->intervention_date)}}
	{{Session::put('geo_location', $negative->geo_location)}}
	{{Session::put('series',  $negative->series)}}
	{{Session::put('support', $negative->support)}}
	{{Session::put('media', $negative)}}
@elseif($type == 'slides')
	@section('title', 'Slides')
	@section('navtitle', 'Slides')
	@section('backurl', URL::to('/gslides'))
	{{Session::put('title', $slide->code)}}
	{{Session::put('tag_1', "DESCRIÇÃO")}}
	{{Session::put('description', $slide->description)}}
	{{Session::put('code', $slide->code)}}
	{{Session::put('tag_2', "DATA DE INTERVENÇÃO")}}
	{{Session::put('published_at', $slide->intervention_date)}}
	{{Session::put('geo_location', $slide->geo_location)}}
	{{Session::put('series',  $slide->series)}}
	{{Session::put('support', $slide->support)}}
	{{Session::put('media', $slide)}}
@elseif($type == 'writings')
	@section('title', $section)
	@section('navtitle', $section)
	@section('backurl', URL::to('/gsection/'.$section))
	{{Session::put('title', $writing->title)}}
	{{Session::put('tag_1', "DESCRIÇÃO")}}
	{{Session::put('description', $writing->description)}}
	{{Session::put('code', $writing->code)}}
	{{Session::put('tag_2', "PUBLICADO EM")}}
	{{Session::put('published_at', $writing->published_at)}}
	{{Session::put('support', $writing->support)}}
	{{Session::put('geo_location', $writing->geo_location)}}
	{{Session::put('series',  $writing->series)}}
	{{Session::put('media', $writing)}}
@endif

<!-- Get body  and clear it -->
@section('body')

	 @section('navbar-menu')
	   	<li><!-- Modal Trigger -->
  			<a class="modal-trigger" href="#modal1"><i class="info-button material-icons white-text">info</i></a>
  		</li>
     @endsection

<div class="custom-side-nav">
	<div class="container">
		<div class="custom-side-nav-content row">
			<!--
			<div class="col s12 m12 l12 selected">
				<a href="#">
					<div class="thumb-card white-text">
					<img src="http://localhost/gallery/public/img/photos/front/001.jpg">
				</div>
				</a>
			</div>
			-->
		</div>
	</div>
</div>

<a href="#" data-activates="slide-out" class="button-collapse hide-on-large-only white-text fixed" style="display: absolute"><i class="material-icons">menu</i></a>

<header id="current-media" data-code="{{Session::get('media')->code}}" onload="loadPics()">
</header>

<main class="valign-wrapper">
	<div class="main-side">
	<div class="container">
		<div class="row no-margin-bottom center-align">
			<div class="col s12 m12 l12">
				<h4 class="no-margin-top title white-text">{{Session::get('title')}}</h4>
			</div>
		</div>
		<div class="row valign-wrapper">
			<div class="col m12">
				<div id="prev-button">
					<a href="#" class="circle-base center">
						<i class="arrow-button material-icons white-text">navigate_before</i>
					</a>
				</div>
				<div class="slider">
					<ul class="slides">
						<!--
						<li>
							<img width="100%" src="{{URL::to('/img/photos/front/001.jpg')}}">
					    </li>
						-->	
					</ul>
				</div>
				<div id="next-button">
					<a class="circle-base center" href="#">
						<i class="arrow-button material-icons white-text">navigate_next</i>
					</a>
				</div>
			</div>
		<!--
			<div  id="prev-button" class="col m1"><a href="#" class="circle-base center"><i class="arrow-button material-icons white-text">navigate_before</i></a></div>
			<div class="col m10">
				<div class="slider">
					<ul class="slides">
					-->
						<!--
						<li>
							<img width="100%" src="{{URL::to('/img/photos/front/001.jpg')}}">
					    </li>
						-->
					<!--
					</ul>
				</div>
			</div>
			<div id="next-button" class="col m1 right-align"><a  class="circle-base center" href="#"><i class="arrow-button material-icons white-text">navigate_next</i></a></div>
		-->
		</div>
		<h6 class="counter grey-text center-align">1/{{$amount}}</h6>
		<div class="row">
			<div class="col s12 m12 l12">
				<p class="label white-text center flow-text">{{Session::get('description')}} (Aqui vai a legenda da foto, pode ser um texto um pouco grande, talvez ultrapasse mais de duas linhas)</p>
			</div>
		</div>
	</div>

	<!-- Modal Structure -->
	  <div id="modal1" class="modal bottom-sheet">
	    <div class="modal-content">
	      <div class="container">
	      	<div class="row">
	      		<div class="col s12 m12 l12">
	     			<h4 class="modal-title">
	     				@if(Session::has('title'))
	     					{{Session::get('title')}}
	     				@else
	     					-
	     				@endif
	     			</h4>
	      		</div>
	      	</div>
	      	<div class="row">
	      		<div class="col s6 m1 l1">
	      			<p class="modal-primary-text">CÓDIGO</p>
	      			<p class="modal-code modal-secondary-text">
	      				@if(Session::has('code'))
	     					{{Session::get('code')}}
	     				@else
	     					-
	     				@endif
	      			</p>
	      		</div>
	      		<div class="col s6 m2 l2">
	      			<p class="modal-published-at-label modal-primary-text">
	      				@if(Session::has('tag_2'))
	     					{{Session::get('tag_2')}}
	     				@else
	     					"PUBLICADO EM"
	     				@endif
	      			</p>
	      			<p class="modal-published-at modal-secondary-text">
	      				@if(Session::has('published_at'))
	     					{{Session::get('published_at')}}
	     				@else
	     					-
	     				@endif
	      			</p>
	      		</div>
	      		<div class="col s6 m3 l3">
	      			<p class="modal-primary-text">LOCALIZAÇÃO</p>
	      			<p class="modal-geo-location modal-secondary-text">
	      				@if(Session::has('geo_location'))
	     					{{Session::get('geo_location')}}
	     				@else
	     					-
	     				@endif
	      			</p>
	      		</div>
	      		<div class="col s6 m3 l3">
	      			<p class="modal-primary-text">SUPORTE</p>
	      			<p class="modal-support modal-secondary-text">
	      				@if(Session::has('support'))
	     					{{Session::get('support')}}
	     				@else
	     					-
	     				@endif
	      			</p>
	      		</div>
	      		<div class="col s6 m3 l3">
	      			<p class="modal-primary-text">SÉRIE</p>
	      			<p class="modal-series modal-secondary-text">
	      				@if(Session::has('series'))
	     					{{Session::get('series')}}
	     				@else
	     					-
	     				@endif
	      			</p>
	      		</div>
	      	</div>
	      	<div class="row">
	      		<div class="col s12 m12 l12">
	      			<p class="modal-description-label modal-primary-text">
	      				@if(Session::has('tag_1'))
	     					{{Session::get('tag_1')}}
	     				@else
	     					"DESCRIÇÃO"
	     				@endif
	      			</p>
	      			<p class="modal-description modal-secondary-text">
	      				@if(Session::has('description'))
	     					{{Session::get('description')}}
	     				@else
	     					-
	     				@endif
	      			</p>
	      		</div>
	      	</div>
	      </div>
	    </div>
	    <div class="modal-footer">
	      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Fechar</a>
	    </div>
	  </div>

	</div>
</main>

<footer>
</footer>

@endsection