@extends('layout.baselayout')

@section('title', 'Administrador')

@section('side-panel-content')

    <ul id="side-panel-list" class="collection with-header">
        <li id="panel-title" class="collection-header pink-text text-lighten-1"><h4>Fotos</h4></li>
        @if(!empty($photos))
             @foreach($photos as $photo)
                <li id="photo-{{$photo->id}}" class="collection-item avatar"   data-value='{{$photo}}' >
                <i class="material-icons circle">photo</i>
                <span class="title">{{$photo->code}}</span>
                <p>{{$photo->title}}<br>
                {{$photo->updated_at}}
                </p>
                </li>
            @endforeach            
        @endif
    </ul>

    <script type="text/javascript">
       Materialize.showStaggeredList('#side-panel-list');
    </script>

@endsection

@section('main-content')
    
    <div class="container">

    <div class="row">

        <ul id="tabs-add" class="tabs center tabs-fixed-width" swipeable="true">
            <li class=" tab col s3 m4"><a class="active" onClick="refreshPhotoList()" href="#add-photo-tab">Fotos</a></li>
            <li class=" tab col s3 m4"><a  onClick="refreshNegativeList()" href="#add-negative-tab">Negativos</a></li>
            <li class=" tab col s3 m4"><a  onClick="refreshSlideList()" href="#add-slide-tab">Slides</a></li>
            <li class=" tab col s3 m4"><a onClick="refreshWritingList()" href="#add-writing-tab">Escritos</a></li>
        </ul>

        <div id="add-photo-tab" class="col s12">@include('tabs.addphoto')</div>
        <div id="add-negative-tab" class="col s12">@include('tabs.addnegative')</div>
        <div id="add-slide-tab" class="col s12">@include('tabs.addslide')</div>
        <div id="add-writing-tab" class="col s12">@include('tabs.addwriting')</div>

    </div>

    </div>

@endsection