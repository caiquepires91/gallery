<div id="add-negative" class="card-panel center">
  {{ Form::open(['method'=>'post','url'=>'negative.add', 'files'=>true, 'id'=>'negative-form'])}}

    <div class="row" >
      <div class="input-field col s12 m3">
      {{ Form::text('code', null, ['class'=>'', 'data-error'=>'']) }}
      {{ Form::label('code', 'Código')}}
      </div>

      <div class="file-field input-field col s12 m9">

        <div class="waves-effect blue darken-4 white-text waves-blue btn">
          <span>Negativo</span>
          {{ Form::file('path', ['enctype'=>'multipart/form-data', 'accept'=>'image/*'])}}
        </div>

        <div class="file-path-wrapper">
          {{ Form::text('path', null, ['class'=>'file-path validate', 'placeholder'=>"Upload negativo"])}}
        </div>
      
      </div>

      <div class="input-field col s12 m6">
      {{ Form::text('collection', null, ['class'=>''])}}
      {{ Form::label('collection', 'Localização no Acervo')}}
      </div>

       <div class="input-field col s12 m6">
      {{ Form::text('number', null, ['class'=>''])}}
      {{ Form::label('number', 'Número do Negativo')}}
      </div>

      <div class="input-field col s12 m12">
      {{ Form::textarea('description', null, ['class'=>'materialize-textarea'])}}
      {{ Form::label('description', 'Descrição')}}
      </div>

      <div class="input-field col s12 m12">
      {{ Form::textarea('back_note', null, ['class'=>'materialize-textarea'])}}
      {{ Form::label('back_note', 'Anotações Envelope')}}
      </div>

      <div class="input-field col s12 m6">
      {{ Form::text('geo_location', null, ['class'=>''])}}
      {{ Form::label('geo_location', 'Localização Geográfica')}}
      </div>

      <div class="input-field col s12 m6">
      {{ Form::text('conservation', null, ['class'=>''])}}
      {{ Form::label('conservation', 'Estado de Conservação')}}
      </div>

      <div class="input-field col s12 m12">
      {{ Form::textarea('observation', null, ['class'=>'materialize-textarea'])}}
      {{ Form::label('observation', 'Observações')}}
      </div>

      <div class="input-field col s12 m6">
      {{ Form::text('brand', null, ['class'=>''])}}
      {{ Form::label('brand', 'Marca')}}
      </div>

       <div class="input-field col s12 m6">
      {{ Form::text('color', null, ['class'=>''])}}
      {{ Form::label('color', 'Cor')}}
      </div>

      <div class="input-field col s12 m6">
      {{ Form::text('intervention_date', null, ['class'=>'datepicker'])}}
      {{ Form::label('intervention_date', 'Data de Intervenção')}}
      </div>

    </div>

    <div class="row">
      <div class="col s12 center">
      <p class='pink-text text-lighten-1'>Obs.: Alguns atributos são obrigatórios, e precisam ser preenchidos antes de adicionar.</p>
      <div class="divider"></div>
      </div>
    </div>

    {{ Form::submit('Adicionar', ['class'=>'waves-effect blue darken-4 white-text waves-blue btn-flat', 'id'=>'add-negative-btn']) }}

{{ Form::close() }}
</div>