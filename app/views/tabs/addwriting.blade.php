<div id="add-writing" class="card-panel center">
  {{ Form::open(['method'=>'post','url'=>'writing.add', 'files'=>true, 'id'=>'writing-form'])}}

      <div class="row" >
      <div class="input-field col s12 m3">
      {{ Form::text('code', null, ['class'=>'', 'data-error'=>'']) }}
      {{ Form::label('code', 'Código')}}
      </div>

      <div class="file-field input-field col s12 m9">

        <div class="waves-effect blue darken-4 white-text waves-blue btn">
          <span>Escrito</span>
          {{ Form::file('path', ['enctype'=>'multipart/form-data', 'accept'=>'image/*'])}}
        </div>

        <div class="file-path-wrapper">
          {{ Form::text('path', null, ['class'=>'file-path validate', 'placeholder'=>"Upload escrito"])}}
        </div>
      
      </div>

      <div class="input-field col s12 m6">
      {{ Form::text('title', null, ['class'=>''])}}
      {{ Form::label('title', 'Título')}}
      </div>

      <div class="input-field col s12 m6">
      {{ Form::text('author', null, ['class'=>''])}}
      {{ Form::label('author', 'Autor')}}
      </div>

      <div class="input-field col s12 m6">
      {{ Form::text('sender', null, ['class'=>''])}}
      {{ Form::label('sender', 'Remetente')}}
      </div>

      <div class="input-field col s12 m6">
      {{ Form::text('published_at', null, ['class'=>'datepicker'])}}
      {{ Form::label('published_at', 'Publicado em')}}
      </div>

      <div class="input-field col s12 m12">
      {{ Form::textarea('description', null, ['class'=>'materialize-textarea'])}}
      {{ Form::label('description', 'Descrição')}}
      </div>

      <div class="file-field input-field col s12 m12">

        <div class="waves-effect blue darken-4 white-text waves-blue btn">
          <span>Ilustração</span>
          {{ Form::file('illustration_path', ['enctype'=>'multipart/form-data', 'accept'=>'image/*'])}}
        </div>

        <div class="file-path-wrapper">
          {{ Form::text('illustration_path', null, ['class'=>'file-path validate', 'placeholder'=>"Upload Ilustração"])}}
        </div>
      
      </div>

    </div>

    <div class="row">
      <div class="col s12 center">
      <p class='pink-text text-lighten-1'>Obs.: Alguns atributos são obrigatórios, e precisam ser preenchidos antes de adicionar.</p>
      <div class="divider"></div>
      </div>
    </div>

    {{ Form::submit('Adicionar', ['class'=>'waves-effect blue darken-4 white-text waves-blue btn-flat', 'id'=>'add-writing-btn']) }}

{{ Form::close() }}
</div>