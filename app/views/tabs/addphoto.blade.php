<div id="add-photo" class="card-panel center">
  {{ Form::open(['method'=>'post','url'=>'photo.add', 'files'=>true, 'id'=>'photo-form'])}}
  
    <div class="row" >
      <div class="input-field col s12 m3">
      {{ Form::text('code', null, ['class'=>'', 'data-error'=>'']) }}
      {{ Form::label('code', 'Código')}}
      </div>

      <div class="input-field col s12 m9">
      {{ Form::text('title', null, ['class'=>''])}}
      {{ Form::label('title', 'Título')}}
      </div>

      <div class="input-field col s12 m12">
      {{ Form::text('label', null, ['class'=>''])}}
      {{ Form::label('label', 'Legenda')}}
      </div>

      <div class="file-field input-field col s12 m6">

        <div class="waves-effect blue darken-4 white-text waves-blue btn">
          <span>Frente</span>
          {{ Form::file('front_path', ['enctype'=>'multipart/form-data', 'accept'=>'image/*'])}}
        </div>

        <div class="file-path-wrapper">
          {{ Form::text('front_file_name', null, ['class'=>'file-path validate', 'placeholder'=>"Upload foto frente"])}}
        </div>
      
      </div>

       <div class="file-field input-field col s12 m6">

        <div class="waves-effect blue darken-4 white-text waves-blue btn">
          <span>Verso</span>
          {{ Form::file('back_path', ['enctype'=>'multipart/form-data', 'accept'=>'image/*'])}}
        </div>

        <div class="file-path-wrapper">
          {{ Form::text('back_file_name', null, ['class'=>'file-path validate', 'placeholder'=>"Upload foto verso"])}}
        </div>
      
      </div>

      <div class="input-field col s6 m3">
      {{ Form::number('height', null, ['class'=>'', 'min'=>'1'])}}
      {{ Form::label('height', 'Altura (cm)')}}
      </div>

      <div class="input-field col s6 m3">
      {{ Form::number('width', null, ['class'=>'', 'min'=>'1'])}}
      {{ Form::label('width', 'Largura (cm)')}}
      </div>

      <div class="input-field col s12 m6">
      {{ Form::text('framing', null, ['class'=>''])}}
      {{ Form::label('framing', 'Enquadramento')}}
      </div>

      <div class="input-field col s12 m12">
      {{ Form::text('conservation', null, ['class'=>''])}}
      {{ Form::label('conservation', 'Estado de Conservação')}}
      </div>

      <div class="input-field col s12 m12">
      {{ Form::textarea('front_note', null, ['class'=>'materialize-textarea'])}}
      {{ Form::label('front_note', 'Anotações Frente')}}
      </div>

      <div class="input-field col s12 m12">
      {{ Form::textarea('back_note', null, ['class'=>'materialize-textarea'])}}
      {{ Form::label('back_note', 'Anotações Verso')}}
      </div>

      <div class="input-field col s12 m6">
      {{ Form::text('suggested_treatment', null, ['class'=>''])}}
      {{ Form::label('suggested_treatment', 'Tratamento Sugerido')}}
      </div>

      <div class="input-field col s12 m6">
      {{ Form::text('executed_treatment', null, ['class'=>''])}}
      {{ Form::label('executed_treatment', 'Tratamento Executado')}}
      </div>

      <div class="input-field col s12 m12">
      {{ Form::textarea('observation', null, ['class'=>'materialize-textarea'])}}
      {{ Form::label('observation', 'Observações')}}
      </div>

      <div class="input-field col s12">
      {{ Form::text('keeper', null, ['class'=>''])}}
      {{ Form::label('keeper', 'Proprietário')}}
      </div>

      <div class="input-field col s12 m6">
      {{ Form::text('intervention', null, ['class'=>'datepicker'])}}
      {{ Form::label('intervention', 'Data de Intervenção')}}
      </div>

      <div class="input-field col s12 m6">
      {{ Form::text('published_at', null, ['class'=>'datepicker'])}}
      {{ Form::label('published_at', 'Publicado em')}}
      </div>

    </div>

    <div class="row">
      <div class="col s12 center">
      <p class='pink-text text-lighten-1'>Obs.: Alguns atributos são obrigatórios, e precisam ser preenchidos antes de adicionar.</p>
      <div class="divider"></div>
      </div>
    </div>

    {{ Form::submit('Adicionar', ['class'=>'waves-effect blue darken-4 white-text waves-blue btn-flat', 'id'=>'add-photo-btn']) }}

{{ Form::close() }}
</div>