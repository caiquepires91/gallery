<!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>

      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
      <script src="js/gallery.js"></script>
      
      <title>Galeria - @yield('title')</title>
    </head>

    <body>

      @section('sidebar')
        <nav class="blue darken-4" role="navigation">
        <div class="nav-wrapper container"><a id="logo-container" href="" class="brand-logo">Galeria</a>
          
          @if(Session::has('user'))
          <!-- Enter here navbar items for logged users -->

          <ul class="right hide-on-med-and-down">
            <li><a href="{{URL::to('/logout')}}">Sair</a></li>
          </ul>

          <ul id="nav-mobile" class="side-nav">
            <li><a href="{{URL::to('/logout')}}">Sair</a></li>
          </ul>

          <ul class="right hide-on-med-and-down">
            <li><a href="#modalRegister" data-toggle="modal" class="modal-trigger">Registrar</a></li>
          </ul>

          <ul id="nav-mobile" class="side-nav">
            <li><a href="#modalRegister" data-toggle="modal" class="modal-trigger">Registrar</a></li>
          </ul>

          @else
          <!-- Enter here navbar items for NOT logged users -->

          @endif

          <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
        </div>
        </nav>

      @show

      <div class="section no-pad-bot" id="index-banner">
      	<!-- Page Layout here -->
    		<div class="row">

    		<div class="col s12 m4 l3 " bgcolor="#f1f1f1"> 
    			@yield('side-panel-content')
    		</div>

    		<div class="col s12 m8 l9">
    			@yield('main-content')
    		</div>

    		</div>
      </div>

      @section('footer')
        <footer class="page-footer transparent">
        </footer>
      @show

    </body>
  </html>