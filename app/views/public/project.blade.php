<!DOCTYPE html>
<html>
<head>
	<!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <!--Import style.css-->
    <link type="text/css" rel="stylesheet" href="css/project.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="css/public-gallery.css"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Acervo Euvaldo - @yield('title', 'Sobre o Projeto')</title>

    <!-- Set back button -->
	@section('logo', 'arrow_back')

    <!-- Check here what button was hit and decide the titles-->
	@section('title', 'Sobre o Projeto')
	@section('navtitle', 'Sobre o Projeto')
</head>
<body>
	<header>
		<nav>
		    <div class="nav-wrapper">
		    	<div class="left">
		    		<a href="{{URL::to('/')}}" class="brand-logo"><i class='material-icons back-button'>@yield('logo')</i>@yield('navtitle', 'Sobre o Projeto')</a>
		    	</div>
	     	</div>
  		</nav>
	</header>

	<main>
		<div class="container">
		<div class="row">
			<div class="col s12 m12">
			    <h2 class="header white-text">Sobre o Projeto</h2>
			    <div class="card horizontal">
		    		<div id="project-card-img" class="card-image about-card-image">
				       <!-- <img src="{{URL::to('img/helper/public/about/project.jpg')}}"> -->
			      	</div>
			      <div class="card-stacked">
			        <div class="card-content">
			          <p class="flow-text" style="text-align: justify;">{{$project->introduction}}</p>
			        </div>
			      </div>
			    </div>
		  	</div>

			<div class="col s12 m12">
			    <div class="card horizontal">
			      <div class="card-stacked">
			        <div class="card-content">
			          <p class="flow-text" style="text-align: justify;">{{$project->text}}</p>
			        </div>
			      </div>
			    </div>
		  	</div>

		  	<div class="col s12 m12">
			    <h2 class="header white-text">Sobre a Equipe</h2>
			    <div class="card horizontal">
		    		<div id="group-card-img" class="card-image about-card-image">
				        <!--<img src="{{URL::to('img/helper/public/about/group.jpg')}}">-->
			      	</div>
			      <div class="card-stacked">
			        <div class="card-content">
			          <p class="flow-text">{{$group->introduction}}</p>
			        </div>
			      </div>
			    </div>
		  	</div>

			<div class="col s12 m12">
			    <div class="card horizontal">
			      <div class="card-stacked">
			        <div class="card-content">
			          <p class="flow-text" style="text-align: justify;">{{$group->text}}</p>
			        </div>
			      </div>
			    </div>
		  	</div>
		</div>
		</div>
	</main>

	<footer>
	</footer>

	 <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/project.js"></script>
</body>
</html>

<!--

	 <img src="{{URL::to('img/photos/front/thumbnails/001.jpg')}}">

<div class="project">
					<div class="project-image center-align vagline-wrapper">
						<img src="{{URL::to('img/photos/front/thumbnails/001.jpg')}}">
					</div>
					<div class="project-content">
						<h4 class="project-title">Sobre o Acervo</h4>
						<h6 class="project-subtitle">Subtítulo</h6>
						<p class="flow-text">One common flaw we've seen in many frameworks is a lack of support for truly responsive text. While elements on the page resize fluidly, text still resizes on a fixed basis. To ameliorate this problem, for text heavy pages, we've created a class that fluidly scales text size and line-height to optimize readability for the user. Line length stays between 45-80 characters and line height scales to be larger on smaller screens.

	To see Flow Text in action, slowly resize your browser and watch the size of this text body change! Use the button above to toggle off/on flow-text to see the difference!</p>
					</div>
				</div> 

-->