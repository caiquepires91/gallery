<!DOCTYPE html>
<html>
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <!--Import style.css-->
    <link type="text/css" rel="stylesheet" href="css/public.css"  media="screen,projection"/>
    <!-- import some design icons -->
    <link href="css/materialdesignicons.min.css" media="all" rel="stylesheet" type="text/css"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Acervo Euvaldo - @yield('title', 'Início')</title>
  </head>

  <body onselectstart="return false" >

    @section('navbar')
    @show

    @section('sidenav')
    @show

    @section('body')

    <ul id="slide-out" class="side-nav fixed section table-of-contents">
      <div class="custom-side-nav">
    <li><a class="white-text valign-wrapper" href="#beginning"><i class="material-icons white-text">home</i>Início</a></li>
    <li><a class="white-text valign-wrapper" href="#project"><i class="material-icons white-text">photo_camera</i>Projeto</a></li>
		<li><a class="white-text valign-wrapper" href="#photos"><i class="material-icons white-text">photo</i>Fotos</a></li>
		<li><a class="white-text valign-wrapper" href="#negatives"><i class="material-icons white-text">camera_roll</i>Negativos</a></li>
		<li><a class="white-text valign-wrapper" href="#slides"><i class="material-icons white-text">slideshow</i>Diapositivos</a></li>
    <li><a class="white-text valign-wrapper" href="#writings"><i class="material-icons white-text">insert_drive_file</i>Escritos</a></li>
		<li><a class="white-text valign-wrapper" href="#calendar"><i class="material-icons white-text">today</i>Eventos</a></li>
    <li><a class="white-text valign-wrapper" href="#contact"><i class="material-icons white-text">phone</i>Contato</a></li>
    <li><a class="white-text valign-wrapper" href="#marks"><i class="white-text mdi mdi-domain mdi-24px"></i>Apoio</a></li>
    </div>
	</ul>
	<a href="#" data-activates="slide-out" class="button-collapse hide-on-large-only white-text fixed" style="display: absolute"><i class="material-icons">menu</i></a>

    <header>
    </header>

    <main>
      <div class="container">
        <br>                
        <br>                
        <br>

        <!-- beginning section -->      			
        <div id="beginning" class="section scrollspy">
        <div class="row">
            <div class="col s12 m12">
                <div class="slider">
                <ul class="slides hoverable">
                  @foreach ($banners as $banner)
                      <li>
                        <a href='{{$banner->link}}'  target="_blank">
                          <img src="img/helper/banner/{{$banner->key}}.jpg"> <!-- random image -->
                          <div class="caption left-align">
                            <h3>{{$banner->title}}</h3>
                            <h5 class="light grey-text text-lighten-3">{{$banner->subtitle}}</h5>
                          </div>
                        </a>
                      </li>
                  @endforeach
                </ul>
              </div>
            </div>
        </div>
        </div>

        <!-- Project section -->            
  		<div id="project" class="section scrollspy">
  		<div class="row">
    	<div class="col s12 m8">
    		<div class="card hoverable">
    			<div class="card-image card-image-square-big" style="background-image: url('img/photos/front/CU10.jpg');">
            <span class="card-title"><strong>Acervo Euvaldo Filho</strong></span>
    			</div>
    			<div class="card-content">
    				<div class="row">
    					
    					<div class="col m12">
    						<strong>Sobre o Projeto</strong><br>
            		<a class="purple-text text-darken-2" href="{{URL::to('/gproject')}}">saiba mais</a>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    	<div class="col s12 m4">
    		<div class="card hoverable">
    			<div class="card-image card-image-vertical" style="background-image: url('img/helper/public/cards/Euvaldo.jpg');">
            <span class="card-title"><strong>Sobre Euvaldo</strong></span>
    			</div>
    			<div class="card-content">
    				    <a class="purple-text text-darken-2" href="{{url::to('/gbiografy')}}">saiba mais</a>
    			</div>
    		</div>
    	</div>
    	</div>
    	</div>

        <!-- Photos section -->            
      <div id="photos" class="section scrollspy">
      <div class="row">
      <div class="col s12 m12">
        <div class="card hoverable">
          <div class="card-image card-image-horizontal" style="background-image:url('img/helper/public/cards/Impresso.jpg');">
              <span class="card-title"><strong>Fotos</strong></span>
              <a class="btn-floating halfway-fab waves-effect waves-light " href="{{URL::to('/gphotos')}}"><i class="material-icons">photo</i></a>
          </div>
          <div class="card-content">
            <!--<h4 class="thin" style="margin: 0px;">Sobre o acervo</h4>-->
                <p class="purple-text text-darken-2">{{$photoamount}} imagens</p>
          </div>
        </div>
      </div>
      </div>
      </div>

      <!-- Negatives section -->            
    	<div id="negatives" class="section scrollspy">
    	<div class="row">
         	<div class="col s12 m12">
    		<div class="card hoverable">
    			<div class="card-image card-image-horizontal" style="background-image:url('img/helper/public/cards/Negativo.jpg');">
          		<span class="card-title"><strong>Negativos</strong></span>
          		<a class="btn-floating halfway-fab waves-effect waves-light  " href="{{URL::to('/gnegatives')}}"><i class="material-icons">camera_roll</i></a>
    			</div>
    			<div class="card-content">
    				<!--<h4 class="thin" style="margin: 0px;">Sobre o acervo</h4>-->
            		<p class="purple-text text-darken-2">{{$negativeamount}} imagens</p>
    			</div>
    		</div>
    	</div>
    	</div>
    	</div>
    	<div id="slides" class="section scrollspy">
    	<div class="row">
    	<div class="col s12 m6">
    		<div class="card hoverable">
    			<div class="card-image card-image-square" style="background-image: url('img/helper/public/cards/Diapositivo.jpg');">
        		<span class="card-title"><strong>Diapositivos (slides)</strong></span>
        		<a class="btn-floating halfway-fab waves-effect waves-light  " href="{{URL::to('/gslides')}}"><i class="material-icons">slideshow</i></a>
    			</div>
    			<div class="card-content">
    				<!--<h4 class="thin" style="margin: 0px;">Sobre o acervo</h4>-->
            		<p class="purple-text text-darken-2">{{$slideamount}} imagens</p>
    			</div>
    		</div>
    	</div>
    	<div class="col s12 m6">
            <div class="card hoverable card-square">
              <div class="card-content">
                <p>Aqui pode ser inseridas algumas informações sobre o acervo, ou curiosidades. Qualquer tipo de texto. Há 2 abas disponíveis.</p>
              </div>
              <div class="card-tabs">
                <ul class="tabs tabs-fixed-width">
                  <li class="tab"><a href="#test4">Exposição</a></li>
                  <li class="tab"><a class="active" href="#test5">Carta</a></li>
                </ul>
              </div>
              <div class="card-content grey lighten-4">
                <div id="test4">Informação sobre alguma exposição</div>
                <div id="test5">Curiosidade sobre uma carta</div>
              </div>
            </div>
    	</div>
    	</div>
    	</div>

        <!-- Writings section -->            
        <div id="writings" class="section scrollspy">
        <div class="row">
            <div class="col s12 m6">
              <div class="card-panel teal card-square center-align valign-wrapper">
                <a href="{{URL::to('/gpoems')}}">
                <span class="white-text">{{$poem->quotation}}</span>
                <br>
                <p class="text-flow author-name right-align white-text">{{$poem->quotation_author}}</p>
                </a>
              </div>
            </div>

            <div class="col s12 m6">
                <div class="card hoverable">
                <div class="card-image card-image-square" style="background-image:url('img/helper/public/cards/Escritos.jpg');">
                    <span class="card-title"><strong>Escritos</strong></span>
                    <a class="btn-floating halfway-fab waves-effect waves-light" href="{{URL::to('/gwritings')}}"><i class="material-icons">insert_drive_file</i></a>
                </div>
                <div class="card-content">
                    <!--<h4 class="thin" style="margin: 0px;">Sobre o acervo</h4>-->
                    <p class="purple-text text-darken-2">{{$writingamount}} imagens</p>
                </div>
            </div>
            </div>
        </div>
        </div>

    <!-- Calendar section -->            
    <div id="calendar" class="section scrollspy">
		<div class="row">
			<div class="col s12 m12">
                <div class="card hoverable">
                    <div class="card-image waves-effect waves-block waves-light">
                      <img class="activator" src="img/photos/front/CU10.jpg">
                    </div>
                    <div class="card-content">
                      <span class="card-title activator grey-text text-darken-4">Eventos<i class="material-icons right">more_vert</i></span>
                      <p><a href="#">www.sitedesteevento.com</a></p>
                    </div>
                    <div class="card-reveal">
                      <span class="card-title grey-text text-darken-4">Eventos<i class="material-icons right">close</i></span>
                      <p>lista de eventos mostrados em forma de cards</p>
                    </div>
                  </div>
            </div>
	    </div>
        </div>

        <!-- Contact section -->            
        <div id="contact" class="section scrollspy">
        <div class="row">
            <div class="col s12 m12">
                <div class="card horizontal hoverable">
                  <div class="card-image">
                    <img src="img/helper/public/cards/Contatos.jpg">
                  </div>
                  <div class="card-stacked">
                    <div class="card-content">
                      <h5>Acervo Euvaldo Macedo Filho</h5>
                      <br>
                      <div class="valign-wrapper contact-padding"><i class="mdi mdi-facebook-box mdi-24px"></i><p><a href="https://www.facebook.com/acervoeuvaldomacedofilho">Nossa Página do Facebook</a></p></div>
                      <br>
                      <div class="valign-wrapper contact-padding"><i class="material-icons">email</i><p>acervoeuvaldo@gmail.com</p></div>
                      <br>
                      <div class="valign-wrapper contact-padding"><i class="material-icons">phone</i><p>(74) 999-99999</p></div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        </div>   
     </div>
    </main>

    <footer>
      <div id="marks" class="section scrollspy">
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
      <div class="container">
        <div class="row">
          <div class="col s12 m3 center-align">
            <a href="http://www.cultura.gov.br" target="_blank"><img src="http://localhost/gallery/public/img/helper/public/sponsors/log1.png"></a>
            <p class="white-text">Ministério da Cultura</p>
            <a href="http://www.cultura.gov.br" target="_blank"> <p class="white-text">www.cultura.gov.br</p></a>
          </div>
          <div class="col s12 m3 center-align">
           <a href="http://www.facebook.com/abajursolucoes" target="_blank"><img src="http://localhost/gallery/public/img/helper/public/sponsors/log2.png"></a>
            <p class="white-text">Rumos Cutural</p>
            <a href="mailto:abajursolucoes@outlook.com" target="_blank"><p class="white-text">abajursolucoes@outlook.com</p></a>
          </div>
          <div class="col s12 m3 center-align">
            <a href="http://portais.univasf.edu.br" target="_blank"><img src="http://localhost/gallery/public/img/helper/public/sponsors/log3.png"></a>
            <p class="white-text">UNIVASF</p>
            <a href="http://portais.univasf.edu.br" target="_blank"><p class="white-text">portais.univasf.edu.br</p></a>
          </div>
          <div class="col s12 m3 center-align">
            <a href="http://www.sescpe.org.br" target="_blank"><img src="http://localhost/gallery/public/img/helper/public/sponsors/log4.png"></a>
            <p class="white-text">SESC</p>
            <a href="http://www.sescpe.org.br" target="_blank"><p class="white-text">www.sescpe.org.br</p></a>
          </div>
           <div class="col s12 m3 center-align">
            <a href="mailto:jgabrielbrito@live.com" target="_blank"><img src="http://localhost/gallery/public/img/helper/public/sponsors/log5.png"></a>
            <p class="white-text">João Gabriel</p>
            <a href="http://www.facebook.com/JGabrielBrito" target="_blank"><p class="white-text">www.facebook.com/JGabrielBrito</p></a>
          </div>
           <div class="col s12 m3 center-align">
            <a href="mailto:abajursolucoes@outlook.com" target="_blank"><img src="http://localhost/gallery/public/img/helper/public/sponsors/log6.png"></a>
            <p class="white-text">Abajur Soluções em Audiovisual</p>
            <a href="http://www.facebook.com/abajursolucoes" target="_blank"><p class="white-text">www.facebook.com/abajursolucoes</p></a>
          </div>
           <div class="col s12 m3 center-align">
            <a href="http://www.cultura.gov.br" target="_blank"><img src="http://localhost/gallery/public/img/helper/public/sponsors/log7.png"></a>
           <p class="white-text">Ministério da Cultura</p>
            <a href="http://www.cultura.gov.br" target="_blank"> <p class="white-text">www.cultura.gov.br</p></a>
          </div>
          </div>
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
      </div>
      </div>
    </footer>

    @show

    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script src="js/public.js"></script>
  </body>
</html>