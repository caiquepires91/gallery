<!DOCTYPE html>
<html>
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="{{URL::to('css/materialize.min.css')}}"  media="screen,projection"/>

    @section('head')
    <!--Import style.css-->
    <link type="text/css" rel="stylesheet" href="{{URL::to('css/style.css')}}"  media="screen,projection"/>
    @show

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Acervo Euvaldo - @yield('title', 'Apresentação')</title>
  </head>

  <body style="background-color: black;">

    @section('navbar')
      <div class="navbar-fixed">
        <nav>
          <div class="nav-wrapper">
            <div class="my-logo">
              <a href="@yield('backurl',URL::to('/'))" class="brand-logo"><i class='material-icons back-button'>@yield('logo', 'photo_library')</i>@yield('navtitle', 'Acervo Euvaldo')</a>
            </div>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
              @section('navbar-menu')
              @show
            </ul>
          </div>
        </nav>
      </div>
    @show

    @section('body')
    @show

    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="{{URL::to('js/materialize.min.js')}}"></script>
    <script src="{{URL::to('js/show.js')}}"></script>
  </body>
</html>