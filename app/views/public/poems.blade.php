<!DOCTYPE html>
<html>
<head>
	<!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <!--Import style.css-->
    <link type="text/css" rel="stylesheet" href="css/project.css"  media="screen,projection"/>
 	<link type="text/css" rel="stylesheet" href="css/public-gallery.css"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Acervo Euvaldo - @yield('title', 'Poesias')</title>

    <!-- Set back button -->
	@section('logo', 'arrow_back')

    <!-- Check here what button was hit and decide the titles-->
	@section('title', 'Poesias')
	@section('navtitle', 'Poesias')
</head>
<body>
	<header>
		<nav>
		    <div class="nav-wrapper">
		    	<div class="left">
		    		<a href="{{URL::to('/')}}" class="brand-logo"><i class='material-icons back-button'>@yield('logo')</i>@yield('navtitle', 'Poesias')</a>
		    	</div>
	     	</div>
  		</nav>
	</header>

	<main>
		<div class="container">
		<div class="row">
			<div class="col s12 m12">
			    <h2 class="header white-text">Poesia de Hoje</h2>
			    <div class="poem-card card horizontal" style="background-image:'img/photos/front/thumbnails/001.jpg'">
			      <div class="card-stacked">
			        <div class="card-content">
			          <p class="flow-text white-text" style="text-align: justify;">{{$poem->introduction}}</p>
			        </div>
			        
			      </div>
			    </div>
		  	</div>

			<div class="col s12 m12">
			    <div class="card horizontal">
			      <div class="card-stacked">
			        <div class="card-content">
			          <div class="center"><h3>{{$poem->title}}</h3></div>
			          <p class="flow-text" style="text-align: justify;">{{$poem->text}}</p>
			          <br>
			          <p class="flow-text author-name right-align">{{$poem->author}}</p>
			        </div>
			      </div>
			    </div>
		  	</div>
		</div>
		</div>
	</main>

	<footer>
	</footer>

	 <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/project.js"></script>
</body>
</html>