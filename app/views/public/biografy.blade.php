<!DOCTYPE html>
<html>
<head>
	<!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <!--Import style.css-->
    <link type="text/css" rel="stylesheet" href="css/project.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="css/public-gallery.css"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Acervo Euvaldo - @yield('title', 'Sobre Euvaldo')</title>

    <!-- Set back button -->
	@section('logo', 'arrow_back')

    <!-- Check here what button was hit and decide the titles-->
	@section('title', 'Sobre Euvaldo')
	@section('navtitle', 'Sobre Euvaldo')
</head>
<body>
	<header>
		<nav>
		    <div class="nav-wrapper">
		    	<div class="left">
		    		<a href="{{URL::to('/')}}" class="brand-logo"><i class='material-icons back-button'>@yield('logo')</i>@yield('navtitle', 'Sobre Euvaldo')</a>
		    	</div>
	     	</div>
  		</nav>
	</header>

	<main>
		<div class="container">
		<div class="row">
			<div class="col s12 m12">
			    <h2 class="header white-text">Sobre Euvaldo Macedo Filho</h2>
			    <div class="card horizontal">
		    		<div id="euvaldo-card-img" class="card-image about-card-image">
			      	</div>
			      <div class="card-stacked">
			        <div class="card-content">
			          <p class="flow-text" style="text-align: justify;">{{$euvaldo->introduction}}</p>
			        </div>
			      </div>
			    </div>
		  	</div>

			<div class="col s12 m12">
			    <div class="card horizontal">
			      <div class="card-stacked">
			        <div class="card-content">
			          <p class="flow-text" style="text-align: justify;">{{$euvaldo->text}}</p>
			        </div>
			      </div>
			    </div>
		  	</div>
		</div>
		</div>
	</main>

	<footer>
	</footer>

	 <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/project.js"></script>
</body>
</html>