$(document).ready(function(){
	const MEDIAS = "medias";
	var url = $(".panel").data('url');
	var mLength;
	var mMedias = [];
	var mLocalMedias = [];
	var mCurrentIndex;
	
	onOpenStart = function() {
		$('.media img').toggleClass('prop');
		console.log("onOpenStart");
	}
	onOpenEnd = function() {
		console.log("onOpenEnd");
	}
	onCloseStart = function() {
		$('.media img').toggleClass('prop');
		console.log("onCloseStart");
	}
	onCloseEnd = function() {
		console.log("onCloseEnd");
	}

	$('.materialboxed').materialbox({
		inDuration:275,
		outDuration:200,
		onOpenStart: onOpenStart,
		onOpenEnd: onOpenEnd,
		onCloseStart: onCloseStart,
		onCloseEnd: onCloseEnd
	});

	$('.datepicker').datepicker();

	$('.btn-next').click(function(){
		if (!mCurrentIndex) $('.btn-prev').removeClass("disabled"); // if current was the fist, then enable prev btn.
		mCurrentIndex = mCurrentIndex + 1;
		count(mCurrentIndex);
		if (mCurrentIndex + 1 == mLength) $('.btn-next').addClass("disabled");
		loadMedia(mMedias[mCurrentIndex].code);
		console.log("next index:" + mCurrentIndex);
	});

	$('.btn-prev').click(function(){
		if (mCurrentIndex + 1 == mLength) $('.btn-next').removeClass("disabled");
		mCurrentIndex = mCurrentIndex - 1;
		count(mCurrentIndex);
		$('.btn-prev').removeClass("disabled");
		if (!mCurrentIndex) $('.btn-prev').addClass("disabled"); // if current now is the first, then disable prev btn.
		loadMedia(mMedias[mCurrentIndex].code);
		console.log("prev");
	});

	/* 1 - Initiate process */
	init();

	function init() {
		if (typeof(Storage) != "undefined") {
			mMedias = JSON.parse(localStorage.getItem(MEDIAS));
			//console.log("Storage: " + mMedias);
			/* 2 -Prepare media arry for setup */			
			for (var i = 0; i < mMedias.length; i++) {
				image = mMedias[i];
				// Get .jpg out of the name.
				var stringLength = image.name.length;
				var substring = image.name.substring(0, stringLength - 4);
				mMedias[i].code = substring;
				//console.log("IMAGEM " + i + ":" + image.file + " CODE:" + mMedias[i].code);
			}
			/* 3 - Setup */
			setup(mMedias);
		} else {
		    alert("Por favor, atualize seu navegador, ou utilize um navegador mais moderno.");
		}
	}

	function setup(medias) {
		mLength = medias.length;
		mCurrentIndex = 0;
		count(mCurrentIndex);
		if (mLength > 1) {
			$('.btn-next').removeClass("disabled");
		}
		/* 4 - Download media info */
		loadMedia(medias[mCurrentIndex].code);
	}

	function count(index) {
		$('#btn-save').html("Salvar " + mMedias[index].code + '<i class="material-icons right">save</i>');
		index = index + 1;
		$(".counter").text(index + "/" + mLength);
	}

	function loadMedia(code) {
		$.post(url + "/get.by.code", {code:code},
            function(data) {
            	if (data.success) {
					console.log('media:' + data.media);
					mLocalMedias.push(data.media);
					showMedia(data.media);
            	} else {
					Materialize.toast('Não Foi Possível Baixar as Informações!!!', 4000);
            	}
        }).fail(function(){
          console.log("error");
        });
	}

	function showMedia(media) {
		// Check which type of media is: photo, writing, slide, negarive.
		console.log("media: " + media.code);
		$('.media img').attr('src', url + "/img/photos/front/" + media.code + ".jpg");
		if (media.pending) $('#checkbox1').prop('checked', true);
		else $('#checkbox1').prop('checked', false);
		$('#code-input').val(media.code);
		$('#series-input').val(media.series);
		$('#color-input').val(media.color);
		$('#label-input').val(media.label);
		$('#width-input').val(media.width);
		$('#height-input').val(media.height);
		$('#framing-input').val(media.framing);
		$('#conservation-input').val(media.conservation);
		$('#location-input').val(media.geo_location);
		$('#executed-treatment-input').val(media.executed_treatment);
		$('#front-notes-input').val(media.front_note);
		$('#back-notes-input').val(media.back_note);
		$('#intervention-input').val(media.intervention);
		setupDatepicker(media.intervention);
		Materialize.updateTextFields();
	}

	function setupDatepicker(timestamp) {
		if (timestamp === "0000-00-00 00:00:00" || timestamp === "0000-00-00") {
			console.log("there is NO date!");
		} else {
			var date = timestampToDate(timestamp);
			setTimepicker(date);
			console.log("there is a date!");
		}
	}

	function timestampToDate(timestamp) {
		var dateString = timestamp.replace(/\s/g, "T");
		console.log("bs string: " + dateString);
		var date = new Date(dateString);
		console.log("day: " + date);
		return date;
	}

	function setTimepicker(date) {
		var $input = $('.datepicker').pickadate();
		var picker = $input.pickadate('picker');
		picker.set('select', date, { format: 'dd-mm-yyyy' });
	}

	$('#btn-save').click(function() {
		// get checkbox.
		$('#btn-save').toggleClass("disabled");
		var form_data = $('#media-form').serializeArray();
		var checked;
		if ($('#checkbox1').is(":checked"))	{
			checked = 1;
			console.log("checked");
		} else {
			checked = 0;
			console.log("not checked");
		}
		form_data.push({"name":"pending", "value":checked});
		console.log("form_data: " + JSON.stringify(form_data));
		$.post($('#media-form').attr('action'), form_data,
		    function(data) {
		    	if (data.success) {
					Materialize.toast('Informações Salvas Para Imagem ' + data.media.code, 4000);
					showMedia(data.media);
		    	} else {
					Materialize.toast('Não Foi Possível Salvar as Informações!!!', 4000);
		    	}
				$('#btn-save').toggleClass("disabled");
		}).fail(function(){
		  console.log("error");
		});
		return false;
	});

});