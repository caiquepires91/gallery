$(document).ready(function(){

	var $base_url = "/gallery/public/";

	Materialize.fadeInImage('body');
   // $('body').css('background-image', 'url(/gallery/public/img/helper/dashboard/background-4.jpg)');

	$('.button-collapse').sideNav({
      menuWidth: 300, // Default is 300
      edge: 'left', // Choose the horizontal origin
      closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
      draggable: true, // Choose whether you can drag to open on touch screens,
      onOpen: function(el) {}, // A function to be called when sideNav is opened
      onClose: function(el) {}, // A function to be called when sideNav is closed
    });

	// Initialize collapse button
  	$(".button-collapse").sideNav();
  	// Initialize collapsible (uncomment the line below if you use the dropdown variation)
  	$('.collapsible').collapsible();

	$('.datepicker').pickadate({
		format: 'yyyy-mm-dd',
	    selectMonths: true, // Creates a dropdown to control month
	    selectYears: 80, // Creates a dropdown of 15 years to control year,
	    today: 'Hoje',
	    clear: 'Limpar',
	    monthsFull: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Março', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    	weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
	    close: 'Ok',
	    closeOnSelect: false // Close upon selecting a date,
  	});

  	$('#photo-form').submit(function(){
  		var form_data = new FormData(document.getElementById('photo-form'));
  		var panel_list = $('#side-panel-list');

	    $.ajax({
	      url: $('#photo-form').attr('action'),
	      type: 'POST',
	      contentType: false, // Important.
	      processData: false, // Important.
	      enctype: 'multipart/form-data',
	      data : form_data,
	      success: function(data){	
	      	var photo = data.photo;
	        console.log('photo-form submitted.' + data.success);
	        $('#side-panel-list li:eq(0)').after(
	        	"<li id='photo-"+ photo.id +"' class='collection-item avatar'  data-value='"+ JSON.stringify(photo) +"'>" +
	                "<i class='material-icons circle'>photo</i>" +
	                "<span class='title'>"+ photo.code +"</span>" +
	                "<p>"+ photo.title +"<br>" +
	            		photo.updated_at +
	                "</p>" +
                "</li>"
	        );
	        
	        Materialize.toast('Foto ' + photo.code + ' adicionada.', 4000);
	        Materialize.showStaggeredList('#side-panel-list');
	        Materialize.updateTextFields();
	        window.scrollTo(0, 0);
	      }
	    });

	    // It is important to return 'false' to prevent the page to load again with echo $result came from controller.
    	return false; 
	});

	$('#negative-form').submit(function(){
		var form_data = new FormData(document.getElementById('negative-form'));
  		var panel_list = $('#side-panel-list');

	    $.ajax({
	      url: $('#negative-form').attr('action'),
	      type: 'POST',
	      contentType: false, // Important.
	      processData: false, // Important.
	      enctype: 'multipart/form-data',
	      data: form_data,
	     success: function(data){	
	      	var negative = data.negative;
	        console.log('negative-form submitted.' + data.success);
	        $('#side-panel-list li:eq(0)').after(
	        	"<li id='negative-"+ negative.id +"' class='collection-item avatar' data-value='"+JSON.stringify(negative)+"'>" +
	                "<i class='material-icons circle'>camera_roll</i>" +
	                "<span class='title'>"+ negative.code +"</span>" +
	                "<p>"+ negative.number +"<br>" +
	            		negative.updated_at +
	                "</p>" +
                "</li>"
	        );
	        
	       	Materialize.toast('Negativo ' + negative.code + ' adicionado.', 4000);
	        Materialize.showStaggeredList('#side-panel-list');
	        Materialize.updateTextFields();
	        window.scrollTo(0, 0);
	      }
	    });
	    // It is important to return 'false' to prevent the page to load again with echo $result came from controller.
    	return false; 
	});

	$('#slide-form').submit(function(){
		var form_data = new FormData(document.getElementById('slide-form'));
  		var panel_list = $('#side-panel-list');

	    $.ajax({
	      url: $('#slide-form').attr('action'),
	      type: 'POST',
	      contentType: false, // Important.
	      processData: false, // Important.
	      enctype: 'multipart/form-data',
	      data: form_data,
	     success: function(data){	
	      	var slide = data.slide;
	        console.log('negative-form submitted.' + data.success);
	        $('#side-panel-list li:eq(0)').after(
	        	"<li id='slide-"+ slide.id +"' class='collection-item avatar' data-value='"+JSON.stringify(slide)+"'>" +
	                "<i class='material-icons circle'>slideshow</i>" +
	                "<span class='title'>"+ slide.code +"</span>" +
	                "<p>"+ slide.brand +"<br>" +
	            		slide.updated_at +
	                "</p>" +
                "</li>"
	        );
	        
	       	Materialize.toast('Slide ' + slide.code + ' adicionado.', 4000);
	        Materialize.showStaggeredList('#side-panel-list');
	        Materialize.updateTextFields();
	        window.scrollTo(0, 0);
	      }
	    });
	    // It is important to return 'false' to prevent the page to load again with echo $result came from controller.
    	return false; 
	});

	$('#writing-form').submit(function(){
  		var form_data = new FormData(document.getElementById('slide-form'));
  		var panel_list = $('#side-panel-list');

	    $.ajax({
	      url: $('#writing-form').attr('action'),
	      type: 'POST',
	      contentType: false, // Important.
	      processData: false, // Important.
	      enctype: 'multipart/form-data',
	      data : form_data,
	      success: function(data){	
	      	var writing = data.writing;
	        console.log('writing-form submitted.' + data.success);
	        $('#side-panel-list li:eq(0)').after(
	        	"<li id='writing-"+ writing.id +"' class='collection-item avatar' data-value='"+JSON.stringify(writing)+"'>" +
	                "<i class='material-icons circle'>insert_drive_file</i>" +
	                "<span class='title'>"+ writing.code +"</span>" +
	                "<p>"+ writing.title +"<br>" +
	            		writing.updated_at +
	                "</p>" +
                "</li>"
	        );
	        
		    Materialize.toast('Escrito ' + writing.code + ' adicionado.', 4000);
	        Materialize.showStaggeredList('#side-panel-list');
	        Materialize.updateTextFields();
	        window.scrollTo(0, 0);
	      }
	    });
	    // It is important to return 'false' to prevent the page to load again with echo $result came from controller.
    	return false; 
    });

  	refreshNegativeList = function() {
  		var panel_list = $('#side-panel-list');
  		$.get($base_url + "negative.all", function(data, status){
  			if (status ==  'success') {
  					panel_list.empty();
  					panel_list.append('<li id="panel-title" class="collection-header pink-text text-lighten-1"><h4>Negativos</h4></li>')
  				$.each(data.negatives, function(index, negative) {
  					panel_list.append(
  						"<li id='negative-"+ negative.id +"' class='collection-item avatar' data-value='"+JSON.stringify(negative)+"'>" +
		                //'<img class="materialboxed circle" data-caption="A picture of a way with a group of trees in a park" ' +
		                //' src="'+ negative.path +'">' +
		               		"<i class='material-icons circle'>camera_roll</i>" +
		                "<span class='title'>"+ negative.code +"</span>" +
		                "<p>"+ negative.number +"<br>" +
		            		negative.updated_at +
		                "</p>" +
		                "</li>"
  						);
          		});

          		Materialize.showStaggeredList('#side-panel-list');
          		/* The following is a way to get a field from data-value from an item */
          		//alert($("#negative-9").data('value').id);
  			}
        	
    	});
  	}

  	refreshPhotoList = function() {
  		var panel_list = $('#side-panel-list');
  		$.get($base_url + "photo.all", function(data, status){
  			if (status ==  'success') {
  					panel_list.empty();
  					panel_list.append('<li id="panel-title" class="collection-header pink-text text-lighten-1"><h4>Fotos</h4></li>')
  				$.each(data.photos, function(index, photo) {
  					panel_list.append(
  						"<li id='photo-"+ photo.id +"' class='collection-item avatar'  data-value='"+ JSON.stringify(photo) +"'>" +
		                "<i class='material-icons circle'>photo</i>" +
		                "<span class='title'>"+ photo.code +"</span>" +
		                "<p>"+ photo.title +"<br>" +
		            		photo.updated_at +
		                "</p>" +
		                "</li>"
  						);
          		});

          		Materialize.showStaggeredList('#side-panel-list');
  			}
        	
    	});
  	}

  	refreshSlideList = function() {
  		var panel_list = $('#side-panel-list');
  		$.get($base_url + "slide.all", function(data, status){
  			if (status ==  'success') {
  					panel_list.empty();
  					panel_list.append('<li id="panel-title" class="collection-header pink-text text-lighten-1"><h4>Slides</h4></li>')
  				$.each(data.slides, function(index, slide) {
  					panel_list.append(
  						"<li id='slide-"+ slide.id +"' class='collection-item avatar'  data-value='"+ JSON.stringify(slide) +"'>" +
		                "<i class='material-icons circle'>slideshow</i>" +
		                "<span class='title'>"+ slide.code +"</span>" +
		                "<p>"+ slide.brand +"<br>" +
		            		slide.updated_at +
		                "</p>" +
		                "</li>"
  						);
          		});

          		Materialize.showStaggeredList('#side-panel-list');
  			}
        	
    	});
  	}

  	refreshWritingList = function() {
  		var panel_list = $('#side-panel-list');
  		$.get($base_url + "writing.all", function(data, status){
  			if (status ==  'success') {
  					panel_list.empty();
  					panel_list.append('<li id="panel-title" class="collection-header pink-text text-lighten-1"><h4>Escritos</h4></li>')
  				$.each(data.writings, function(index, writing) {
  					panel_list.append(
  						"<li id='writing-"+ writing.id +"' class='collection-item avatar'  data-value='"+ JSON.stringify(writing) +"'>" +
		                "<i class='material-icons circle'>insert_drive_file</i>" +
		                "<span class='title'>"+ writing.code +"</span>" +
		                "<p>"+ writing.title +"<br>" +
		            		writing.updated_at +
		                "</p>" +
		                "</li>"
  						);
          		});

          		Materialize.showStaggeredList('#side-panel-list');
  			}
        	
    	});
  	}

  	/** Drag and Drop Section **/

/*
  	var isAdvancedUpload = function() {
  		var div = document.createElement('div');
  		return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
	}();

	var $form = $('.box');

	if (isAdvancedUpload) {
	  $form.addClass('has-advanced-upload');
	}

	if (isAdvancedUpload) {
	  var droppedFiles = false;

	  $form.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
	    e.preventDefault();
	    e.stopPropagation();
	  })
	  .on('dragover dragenter', function() {
	    $form.addClass('is-dragover');
	    console.log("drag over!!!");
	  })
	  .on('dragleave dragend drop', function() {
	    $form.removeClass('is-dragover');
	    console.log("drag leave!!!");
	  })
	  .on('drop', function(e) {
	    droppedFiles = e.originalEvent.dataTransfer.files;
	    console.log("dropped!!!");
	  });
	}
	*/

});