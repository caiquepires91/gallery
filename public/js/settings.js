$(document).ready(function(){
	$('.collapsible').collapsible();

	loadData();

	function loadData() {
		dataDiv = $("#settings-data");
		var project = JSON.parse(decodeURIComponent(dataDiv.data("project")));
		var group = JSON.parse(decodeURIComponent(dataDiv.data("group")));
		var euvaldo = JSON.parse(decodeURIComponent(dataDiv.data("euvaldo")));
		var poem = JSON.parse(decodeURIComponent(dataDiv.data("poem")));
		var banner1 = JSON.parse(decodeURIComponent(dataDiv.data("banner-1")));
		var banner2 = JSON.parse(decodeURIComponent(dataDiv.data("banner-2")));
		var banner3 = JSON.parse(decodeURIComponent(dataDiv.data("banner-3")));

		console.log("Project: " + project);

		loadProject(project);
		loadGroup(group);
		loadEuvaldo(euvaldo);
		loadPoem(poem);
		loadBanner(banner1);
		loadBanner(banner2);
		loadBanner(banner3);
	}

	function loadProject(project) {
		$('#project-introduction').val(replacePlus(project.introduction));
        $('#project-text').val(replacePlus(project.text));
	}

	function loadGroup(group) {
		$('#group-introduction').val(replacePlus(group.introduction));
        $('#group-text').val(replacePlus(group.text));
	}

	function loadEuvaldo(euvaldo) {
		$('#euvaldo-introduction').val(replacePlus(euvaldo.introduction));
        $('#euvaldo-text').val(replacePlus(euvaldo.text));
	}

	function loadPoem(poem) {
		$('#poem-introduction').val(replacePlus(poem.introduction));
		$('#poem-text').val(replacePlus(poem.text));
		$('#poem-quotation').val(replacePlus(poem.quotation));
		$('#poem-quotation-author').val(replacePlus(poem.quotation_author));
		$('#poem-title').val(replacePlus(poem.title));
		$('#poem-author').val(replacePlus(poem.author));
	}

	function loadBanner(banner) {
		console.log(banner);
		if(banner.key == "b1") {
			title = $('#banner-title-1');
			subtitle = $('#banner-subtitle-1');
			link = $('#banner-link-1');
		} else if (banner.key == "b2") {
			title = $('#banner-title-2');
			subtitle = $('#banner-subtitle-2');
			link = $('#banner-link-2');
		} else {
			title = $('#banner-title-3');
			subtitle = $('#banner-subtitle-3');
			link = $('#banner-link-3');
		}

		title.val(replacePlus(banner.title));
		subtitle.val(replacePlus(banner.subtitle));
		link.val(replacePlus(banner.link));
	}

	function replacePlus(str) {
		return str.replace(/(?:\+)/g, ' ');
	}

	/** Save Project Data **/
	$('#save-project-btn').click(function() {
      var form_data = $('#settings-project-form').serializeArray();
      $.post($('#settings-project-form').attr('action'), form_data,
                function(data) {
                	if (data.success) {
						console.log('project:' + data.project + " group:" + data.group);
                		var group = data.group;
                		var project = data.project;
                		$('#project-introduction').val(project.introduction);
                		$('#project-text').val(project.text);
                		$('#group-introduction').val(group.introduction);
                		$('#group-text').val(group.text);
						Materialize.updateTextFields();
						Materialize.toast('Informações Salvas.', 4000);
                	} else {
						Materialize.toast('Não Foi Possível Salvar as Informações!!!', 4000);
                	}
            }).fail(function(){
              console.log("error");
            });

      return false;
   });

	/** Save Euvaldo Data **/
	$('#save-euvaldo-btn').click(function() {
      var form_data = $('#settings-euvaldo-form').serializeArray();
      $.post($('#settings-euvaldo-form').attr('action'), form_data,
                function(data) {
                	if (data.success) {
						console.log('euvaldo:' + data.euvaldo);
                		var euvaldo = data.euvaldo;
                		$('#euvaldo-introduction').val(euvaldo.introduction);
                		$('#euvaldo-text').val(euvaldo.text);
						Materialize.updateTextFields();
						Materialize.toast('Informações Salvas.', 4000);
                	} else {
						Materialize.toast('Não Foi Possível Salvar as Informações!!!', 4000);
                	}
            }).fail(function(){
              console.log("error");
            });

      return false;
   });

	/** Save Poem Data **/
	$('#save-poem-btn').click(function() {
      var form_data = $('#settings-poem-form').serializeArray();
      $.post($('#settings-poem-form').attr('action'), form_data,
                function(data) {
                	if (data.success) {
						console.log('poem:' + data.poem);
                		var poem = data.poem;
                		$('#poem-introduction').val(poem.introduction);
                		$('#poem-text').val(poem.text);
                		$('#poem-quotation').val(poem.quotation);
                		$('#poem-quotation-author').val(poem.quotation_author);
                		$('#poem-title').val(poem.title);
                		$('#poem-author').val(poem.author);
						Materialize.updateTextFields();
						Materialize.toast('Informações Salvas.', 4000);
                	} else {
						Materialize.toast('Não Foi Possível Salvar as Informações!!!', 4000);
                	}
            }).fail(function(){
              console.log("error");
            });

      return false;
   });

	/** Save Banner Data **/
	$('#save-banner-btn').click(function() {
      var form_data = new FormData($('#settings-banner-form')[0]);
      console.log("form data: " + form_data);
      $.ajax({
	      url: $('#settings-banner-form').attr('action'),
	      type: 'POST',
	      contentType: false, // Important.
	      processData: false, // Important.
	      enctype: 'multipart/form-data',
	      data : form_data,
	      success: function(data){	
	      	if (data.success) {
				console.log('banner:' + data.banner);
        		var banner1 = data.banner1;
        		var banner2 = data.banner2;
        		var banner3 = data.banner3;
        		$('#banner-title-1').val(banner1.title);
        		$('#banner-subtitle-1').val(banner1.subtitle);
        		$('#banner-link-1').val(banner1.link);
        		//$('#banner-file-1').val(banner1.file);
        		$('#banner-title-2').val(banner2.title);
        		$('#banner-subtitle-2').val(banner2.subtitle);
        		$('#banner-link-2').val(banner2.link);
        		//$('#banner-file-2').val(banner2.file);
        		$('#banner-title-3').val(banner3.title);
        		$('#banner-subtitle-3').val(banner3.subtitle);
        		$('#banner-link-3').val(banner3.link);
        		//$('#banner-file-3').val(banner3.file);
				Materialize.updateTextFields();
				Materialize.toast('Informações Salvas.', 4000);
        	} else {
				Materialize.toast('Não Foi Possível Salvar as Informações!!!', 4000);
        	}
	      }
	    }).fail(function(){
          console.log("error");
        });

      return false;
   });

});