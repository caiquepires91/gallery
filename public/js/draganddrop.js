$(document).ready(function() {
	const MEDIAS = "medias";
	var mImagesForm = null;

	$('.slider').slider({
		indicators:false,
		height: $('.slides').height(),
	});

	$('.carousel').carousel({
		duration: 200,
		dist: -100,
		shift: 0,
		padding: 0,
		fullWidth: true,
		indicatior: false,
		noWrap:false,
	});

	$('select').material_select();

	/** Patricia drag and drop section **/
	var dropzone = document.getElementById('dropzone');

	/** 3 - upload image **/
	function loadImage(file) {
		/** 3 - resize thumbnail **/		
		//file = resize();
		var reader = new FileReader();
        reader.onload = function(e){
        	var image = $("<img>");
			image.file = file;
            image.src = e.target.result;
        	//console.log("before: " + image.src);
        	showImage(image, file);
        };
        reader.readAsDataURL(file);
	}

	/** 4 - upload image **/
	function showImage(image, file) {
    	//console.log("image after: " + image.src);
    	var mediaName = file.name;
		// Get .jpg out of the name.
		var stringLength = mediaName.length;
		var substring = mediaName.substring(0, stringLength - 4);
		console.log("on addPicOnDragnddrop:" + substring);

		row = $('#media-row');
		col = $("<div></div>").addClass('col m2');
		col.attr('id', substring); // Uniquefy each column with this.
		parent = $("<div></div>").addClass('parent');
		wrapper = $("<div></div>").addClass('wrapper');
		card = $("<div></div>").addClass('card');
		cardImage = $("<div></div>").addClass('card-image');
    	overlay = $("<div></div>").addClass('overlay');
		object = $("<div></div>").addClass('object');

		// create lower object.
		var img = $('<img>'); //Equivalent: $(document.createElement('img'))
		img.attr('src', image.src);
		img.addClass('thumbnail');
		cardImage.append(img);
		card.append(cardImage);
		wrapper.append(card);
		// this 'object' is the progress bar
		/*
		object.append(
			"<div class=\"preloader-wrapper small active\">" +
		    "<div class=\"spinner-layer spinner-green-only\">" +
		      "<div class=\"circle-clipper left\">" +
		        "<div class=\"circle\"></div>" +
		      "</div><div class=\"gap-patch\">" +
		        "<div class=\"circle\"></div>" +
		      "</div><div class=\"circle-clipper right\">" +
		        "<div class=\"circle\"></div>" +
		      "</div>" +
		    "</div>" +
		  	"</div>"
			);
		overlay.append(object);
		wrapper.append(overlay);
		*/
		parent.append(wrapper);
		col.append(parent);
		col.append('<p>' + substring + '</p>');
		row.append(col);
    }		
				
	function  displayUploads(data) {
		var uploads = document.getElementById('uploads'),
		anchor,
		x;

		// Remove overflow.
		$(".dropzone").removeClass('dropped');

		for(x = 0; x < data.length; x = x+1)
		{
			var image = data[x];

			if (image != null) {
				console.log(image.name);

				// get .jpe out of the name.
				var stringLength = image.name.length;
				var substring = image.name.substring(0, stringLength - 4);
				console.log(substring);

				// removePiFromDragndrop(data[x].name);
				// This will remove objects card after uploaded
				// Obs.: it wont work if there is blank space in the pic's name.
				$("div").remove("#" + substring);

				// TODO: show user list of files uploading. a list.
				anchor = document.createElement('a');
				anchor.href = "http://localhost/gallery/public" + data[x].file;
				anchor.innerText = data[x].name;
				var br = document.createElement('br');
				uploads.appendChild(anchor);
				uploads.appendChild(br);
			}
		}

		// Recreate the message drag and drop.
		var p = $('<p></p>').text('Arraste e solte até 30 imagens .jpeg (max.: 2MB cada) para fazer upload.');
		var i = $('<i></i>').addClass('material-icons').html('add_photo_alternate');
		var div = $('<div></div>').addClass('insidedrop').appendChild(i).appendChild(p);
		$(".dropzone").append(p);
	}
	
	/** 2 - upload image **/
	function getImages(files) {
		var formData = new FormData();
		var x;

		// Delete message from the center.
		$('div').remove(".insidedrop");
		// Add overflow.
		$(".dropzone").addClass("dropped");
		
		for(x = 0; x < files.length; x = x+1)
		{
			formData.append('images[]',files[x]);
			/** 2 - load images to the drag and drops **/
			loadImage(files[x]);
		}

		console.log("getImages:" + files);
		$('#btn-save').removeClass('disabled');
		$('#btn-cancel').removeClass('disabled');

		return formData;
	}

	/** 5 (save) - upload image **/
	function uploadImages(formData) {
		var dropzone = $('#dropzone');
		var type = dropzone.attr("type");
		var url = dropzone.data("url");
		var action = dropzone.data('type'); //change later for 'action'
		$.ajax({
		    url: action,
		    data: formData,
		    xhr: function() {
	                //var myXhr = $.ajaxSettings.xhr();
	                var myXhr = new window.XMLHttpRequest();
	                if(myXhr.upload){
	                    myXhr.upload.addEventListener('progress', progress, false);
	                }
	                return myXhr;
	        },
		    cache: false,
		    contentType: false,
		    processData: false,
		    method: 'POST',
		    type: 'POST', // For jQuery < 1.9
		    success: function(data) {
				console.log(data.length);
				$('.middle-dot').html("&#9679;");
				$('#result').text(data.length + " imagens salvas");
				var btnsDiv = $('#btn-cancel').parent();
				$('#btn-cancel').remove();
				$('#btn-save').remove();
				var done = $('<a></a>').addClass("btn-flat waves-effect waves-light btn white-text");
				done.attr("id", "btn-done");
				done.text("Concluir");
				done.append('<i class="material-icons right">cloud_done</i>');
				var edit = $('<a></a>').addClass("btn-flat waves-effect waves-light btn white-text");
				edit.attr("id", "btn-edit");
				edit.attr("href", url + "/edit/" + type);
				edit.text("Editar")
				edit.append('<i class="material-icons right">edit</i>');
				btnsDiv.append(edit);
				btnsDiv.append(done);
				addEventToBtns();
				for (var i = 0; i < data.length; i++) {
					image = data[i];
					// Get .jpg out of the name.
					var stringLength = image.name.length;
					var substring = image.name.substring(0, stringLength - 4);
					var wrapper = $('#' + substring).find('.wrapper');
					overlay = $("<div></div>").addClass('overlay');
					object = $("<div></div>").addClass('object');
					object.append('<i class="material-icons">cloud_done</i>');
					overlay.append(object);
					wrapper.append(overlay);
					console.log("image " + i + ": " + image.file);
				}
				if (typeof(Storage) != "undefined") {
					localStorage.setItem(MEDIAS, JSON.stringify(data));
				} else {
					edit.addClass("disabled");
				    alert("Por favor, atualize seu navegador, ou utilize um navegador mais moderno.");
				}
	   		}
		});
	}

	function progress(e){
	    if(e.lengthComputable){
	        var max = e.total;
	        var current = e.loaded;
	        var Percentage = (current * 100)/max;
			console.log("progress tota:" + max + " current:" + current);
	        var progressElement = $('#progress');
	        progressElement.text(parseInt(Percentage) + "%");

	        if(Percentage >= 100) {
	          // enable buttons.
	        }
	    }  
	 }
	
	/** 1 - upload image **/
	dropzone.ondrop = function(e)
	{
		e.preventDefault();
		console.log("on drop");
		this.className = 'dropzone';
		/** 1 - begind upload process **/
		mImagesForm = getImages(e.dataTransfer.files);
		console.log("ondrop mImagesForm: " + mImagesForm);
	};

	dropzone.dragenter = function(e) {
		console.log('on drag enter');
	}
	
	dropzone.ondragover = function(e){
		e.preventDefault();
		this.className = 'dropzone dragover';
		console.log("on drag over");
		return false;
	};
	
	dropzone.ondragleave = function(e)
	{
		console.log("on drag leave");
		this.className = 'dropzone';
		return false;
	};

	$('#btn-save').click(function(){
		if (mImagesForm != null) {
			$('#progress').text("0%");
			$('#btn-cancel').addClass('disabled');
			$('#btn-save').addClass('disabled');
			uploadImages(mImagesForm);
		} else {
			console.log("mImagesForm erro: " + mImagesForm);
		}
	});

	$('#btn-cancel').click(function(){
		$('#btn-cancel').addClass('disabled');
		$('#btn-save').addClass('disabled');
		$('#media-row').empty();
		mImagesForm = null;
	});

	function onClickDoneBtn() {
		console.log("done clicked");
		location.reload();
	}

	function onClickEditBtn() {
		console.log("edit clicked");
		//location.reload();
	}

	function addEventToBtns() {
		console.log("addEventToBtns");
		var doneBtn = $('#btn-done');
		var editBtn = $('#btn-edit');
		doneBtn.bind("click", onClickDoneBtn);
		editBtn.bind("click", onClickEditBtn);
	}

});