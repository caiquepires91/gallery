$(document).ready(function(){
	$('.materialboxed').materialbox();

	const BASE_URL = "http://localhost/gallery/public/";
	var count = 0;
	var counter = $('.counter');
	var stored = localStorage['medias'];
	if (stored) myVar = JSON.parse(stored);
	var currentMedia = null;

	loadPics();

	/** DOM manipulation **/
	$('.modal').modal(); // initializing modal.

	$('.slider').slider({
		indicators:false,
		height: $('.slides').height(),
	});

	$('.carousel').carousel({
		duration: 200,
		dist: -100,
		shift: 0,
		padding: 0,
		fullWidth: true,
		indicatior: false,
		noWrap:true,
		onCycleTo: function(ele, dragged) {
			alert($(ele).index());
			console.log("oncycle:" + dragged);
		}
	});

	$('.slider').slider({
		indicators:false,
		height: $('.slides').height(),
	});

    $('.slider').slider('pause');

	var scrollCount = 0;
	var scrollCorrection = 0;
	var isKeyDown = false;
	const SCROLL_INDEX = 120;
	const SCROLL_CORRECTION_INDEX = 0.3;

	$( ".custom-side-nav" ).keydown(function(event) {
		selected = $('.custom-side-nav .selected');
		index = selected.find("div").attr('index');

		if (event.which == 40 || event.which == 39) { // Arrow Down.
			isKeyDown = true;
			$('.custom-side-nav').css('overflow-y', 'scroll'); // show scroll if clicked any item.
			//console.log( "Handler for .keydown() called. ArrowDown" );
			//console.log("selected index: " + selected.find("div").attr('index') + " count: " + count);
			// Check if it's the last element.
			if (index != (count - 1)) {
				$('.custom-side-nav .col').removeClass('selected');
				next = selected.next();
				next.addClass('selected');
				next.find("div").click();

				scrollCount += (SCROLL_INDEX + scrollCorrection);
				scrollCorrection += SCROLL_CORRECTION_INDEX;
				console.log("scroll top: " + scrollCount);
				$('.custom-side-nav').animate({
					scrollTop: scrollCount
				});

				console.log("next!");
			}
		}
		if (event.which == 38  || event.which == 37) { // Arrow Up.
			isKeyDown = true;
			$('.custom-side-nav').css('overflow-y', 'scroll'); // show scroll if clicked any item.
			//console.log( "Handler for .keydown() called. ArrowUp" );
			//console.log("selected index: " + selected.find("div").attr('index'));
			// Check if it's the first elemet.
			if (index != 0) {
				$('.custom-side-nav .col').removeClass('selected');
				prev = selected.prev();
				prev.addClass('selected');
				prev.find("div").click();
				
				scrollCount -= (SCROLL_INDEX + scrollCorrection);
				scrollCorrection -= SCROLL_CORRECTION_INDEX;
				console.log("scroll top: " + scrollCount);
				$('.custom-side-nav').animate({
					scrollTop: scrollCount
				});

				console.log("prev!");
			}
		}
	});

	// Hide scroll from side-bar and set hover again.
	$(".main-side").click(function(){
		$('.custom-side-nav').css('overflow-y', 'hidden'); // hide scroll if clicked any item.
		$('.custom-side-nav').mouseenter(function(){
			$('.custom-side-nav').css('overflow-y', 'scroll'); // show scroll if clicked any item.
		}).mouseleave(function(){
			$('.custom-side-nav').css('overflow-y', 'hidden'); // show scroll if clicked any item.
		});
	});

	/** Function declarations **/
	function show() {
		
		/** fill picture elements **/
		$('.custom-side-nav .col').removeClass('selected');
		photoElement = $(this);
		photoParent = photoElement.parent().parent();
		photoParent.addClass('selected');
		photo = $(this).data('img');
		currentMedia = photo;

		sum = 1;
		sum += + photoElement.attr('index');
		counter.text(sum + "/" + count);
		$('.title').text(photo.title);
		$('.label').text((photo.label == null ? photo.description : photo.label));
		console.log("(show): " + photoElement.attr('id') + " label: " + photo.label);

		/** fill modal elements **/
		modalTitle = $('.modal-title');
		modalCode = $('.modal-code');
		modalPublishedAt = $('.modal-published-at');
		modalPublishedAtLabel = $('.modal-published-at-label');
		modalSupport = $('.modal-support');
		modalDescription = $('.modal-description');
		modalDescriptionLabel = $('.modal-description-label');
		modalSeries = $('.modal-series');
		modalGeoLocation = $('.modal-geo-location');

		modalCode.text((photo.code == null ? "-" : photo.code));

		if (photo.title == null) {
			if (photo.collection == null) {
				modalTitle.text(photo.code);
			} else {
				modalTitle.text(photo.collection);
			}
		} else {
			modalTitle.text(photo.title);
		}

		if (photo.code == null) {
			modalCode.text("-");
		} else {
			modalCode.text(photo.code);
		}

		if (photo.description == null) {
			modalDescriptionLabel.text("LEGENDA");
			if (photo.label == null) {
				modalDescription.text("-");
			} else {
				modalDescription.text(photo.label);
			}			
		} else {
			modalDescriptionLabel.text("DESCRIÇÃO");
			modalDescription.text(photo.description);
		}

		if (photo.published_at == null) {
			modalPublishedAtLabel.text("DATA DE INTERVENÇÃO");
			if (photo.intervention_date == null) {
				modalPublishedAt.text("-");
			} else {
				modalPublishedAt.text(photo.intervention_date);
			}
		} else {
			modalPublishedAtLabel.text("PUBLICADO EM");
			modalPublishedAt.text(photo.published_at);
		}

		if (photo.support == null) {
			modalSupport.text("-");
		} else {
			modalSupport.text(photo.support);
		}

		if (photo.geo_location == null) {
			modalGeoLocation.text("-");
		} else {
			modalGeoLocation.text(photo.geo_location);
		}

		if (photo.series == null) {
			modalSeries.text("-");
		} else {
			modalSeries.text(photo.series);
		}
	}

	$("#prev-button").click(function() {
		currentIndex = $('#' + currentMedia.id).attr('index');
		console.log("current index prev: " + Number(currentIndex));
		if (Number(currentIndex) > 0) {
			prevIndex = Number(currentIndex) - 1;
			$("div[index=" + prevIndex + "]").click();
			console.log("prev, prevIndex: " + prevIndex);
			$('.slider').slider('prev');
			scrollCount -= (SCROLL_INDEX + scrollCorrection);
			scrollCorrection -= SCROLL_CORRECTION_INDEX;
			console.log("scroll top: " + scrollCount);
			$('.custom-side-nav').animate({
				scrollTop: scrollCount
			});
		}
	});

	$("#next-button").click(function() {
		currentIndex = $('#' + currentMedia.id).attr('index');
		console.log("current index next: " + Number(currentIndex));
		if (Number(currentIndex) < count - 1) {
			nextIndex = Number(currentIndex) + 1;
			$("div[index=" + nextIndex + "]").click();
			console.log("next, nextIndex: " + nextIndex);
			$('.slider').slider('next');
			//$('.slider').slider('pause');
			scrollCount += (SCROLL_INDEX + scrollCorrection);
			scrollCorrection += SCROLL_CORRECTION_INDEX;
			console.log("scroll top: " + scrollCount);
			$('.custom-side-nav').animate({
				scrollTop: scrollCount
			});
		}
	});

	function loadPics() {
		// Pause slider
		//$('.slider').slider('pause');
		var slides = $('.slides');
		var sideNavContent = $('.custom-side-nav-content');
		currentMediaCode = $('#current-media').data("code");
		console.log("current media code:" + currentMediaCode);
		$.each(myVar, function(index, photo) {
			if (photo.code == currentMediaCode) {
				currentMedia = photo;
				currentMedia.index = index;
				console.log("found current media: " + currentMedia.index);
			}

			console.log("photo codee: " + photo.code);

			count++;

			sideNavContent.append(
				"<div class=\"col s12 m12 l12\">" +
					"<a href=\"#\">" +
						"<div id=\"" + photo.id + "\" index=" + index + " class=\"thumb-card white-text\">" +
							"<img src=\"http://localhost/gallery/public/img/photos/front/" + photo.code + ".jpg\">" +
						"</div>" +
						"<p class=\"white-text center\">" + 
						photo.code +
						"</p>" +
					"</a>" +
				"</div>"
			);

			slides.append(
				"<li>" +
				"<img class='materialboxed' id=\"show-" + photo.id + "\" src='" + BASE_URL + "img/photos/front/" + photo.code + ".jpg'>" +
			    "</li>"
			);

			$("#" + photo.id).on("click", show); // Add trigger, won't work via html.
			$("#" + photo.id).data('img', photo); //add the whole data to the element key:img.
			/** test puposes **/
			var jsonObject = $("#" + photo.id).data('img');
  			//console.log("jsonObject id:" + jsonObject.code);
    	});
    	counter.text((currentMedia.index + 1) + "/" + count);
    	console.log('myVar: ' + myVar + " lengh:" + count);
    	$("#" + currentMedia.id).click();
	}


});