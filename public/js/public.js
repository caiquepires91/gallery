// Initialize collapse button
// Initialize collapsible (uncomment the line below if you use the dropdown variation)

// uncomment below to disable right click 
//document.oncontextmenu =new Function("return false;");

$('.button-collapse').sideNav({
      menuWidth: 200, // Default is 300
      edge: 'left', // Choose the horizontal origin
      closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
      draggable: true, // Choose whether you can drag to open on touch screens,
      onOpen: function(el) { /* Do Stuff */ }, // A function to be called when sideNav is opened
      onClose: function(el) { /* Do Stuff */ }, // A function to be called when sideNav is closed
    }
 );

$('.collapsible').collapsible();

$(".button-collapse").sideNav();

$('.scrollspy').scrollSpy();

$('.slider').slider();

if (screen.width < 768) {
	console.log("screen width:" + screen.width);
	$(".card").removeClass("horizontal");
}
console.log("screen width not included:" + screen.width);