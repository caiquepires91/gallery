$(document).ready(function(){

	$('select').material_select();
	$('.materialboxed').materialbox();
	Materialize.updateTextFields();

	var is_filter_showing = false;
	$('#filter').click(function(){
		if(is_filter_showing) {
			console.log("hide");
			$('.board').hide();
			is_filter_showing = false;
		} else {
			console.log("show");
			$('.board').show();
			is_filter_showing = true;
		}
	});

	$("#icon_prefix").focus(function() {
		if (!is_filter_showing) {
			$('.board').show();
			is_filter_showing = true;
		}
	});

	$(".main").click(function() {
		if (is_filter_showing) {
			$('.board').hide();
			is_filter_showing = false;
		}
	});

	/** Load data and scroll control**/

	// If 'AMOUNT_LIMIT' changes here, remember to change it in PhotoController.
	const BASE_URL = "/gallery/public/";
	const AMOUNT_LIMIT = 30;

	var medias = [];
	var data = {};
	var scroll_flag = false;
	var search_post = false;
	var row = $('.photo-content');
	data.index = AMOUNT_LIMIT;
	data.attribute = 'title'; // keyup will search on this object key. Check Controller fun search().
	console.log('on start, index:' + data.index + ' scroll flag:' + scroll_flag);

	/** 
	** Handle Filter 
	**/
	$('#filter-title').click(function(){
		if (this.checked) {
			$('#filter-code').prop("checked", !this.checked);
			$('#filter-label').prop("checked", !this.checked);
			data.attribute = 'title';
		} else {
			data.attribute = 'code';
			$('#filter-code').prop("checked", !this.checked);
		}
	});

	$('#filter-code').click(function(){
		if (this.checked) {
			$('#filter-title').prop("checked", !this.checked);
			$('#filter-label').prop("checked", !this.checked);
			data.attribute = 'code';
		} else {
			data.attribute = 'title';
			$('#filter-title').prop("checked", !this.checked);
		}
	});

	$('#filter-label').click(function(){
		if (this.checked) {
			$('#filter-code').prop("checked", !this.checked);
			$('#filter-title').prop("checked", !this.checked);
			data.attribute = 'label';
		} else {
			data.attribute = 'title';
			$('#filter-title').prop("checked", !this.checked);
		}
	});

	/*
	* first identify the page, to decide which action to take.
	*/
	var type = $('.photo-content').attr('type');
	console.log('type: ' + type);
	switch(type) {
		case 'photos':
			url_compliment = "photo.get";
			url_search = "photo.search";
			break;
		case 'negatives':
			url_compliment = "negative.get";
			url_search = "negative.search";
			$('#filter-title').prop('disabled', "disabled");
			$('#filter-label-title').addClass('grey-text');
			break;
		case 'slides':
			url_compliment = "slide.get";
			url_search = "slide.search";
			break;
		case 'writings':
			url_compliment = "writing.get";
			url_search = "writing.search";
			break;
		case 'sections':
			url_compliment = "section.get";
			url_search = "section.search";
			data.section = $('.photo-content').attr('section');
			data.section = decodeURI(data.section);
			$('.brand-logo').attr('href', BASE_URL + "gwritings");
			console.log("I'm in section:" + data.section + " url compliment:" + url_compliment + " url search:" + url_search);
			break;
		default:
			url_compliment = "photo.get";
			url_search = "photo.search";
	}
	getData(url_compliment, data);

	/*
	* Generical functions.
	*/
	$(".reload").click(function(){
		reload();
	});

	function reload() {
		$("#icon_prefix").val('');
		clearData();
		medias.length = 0;
		localStorage.removeItem("medias");
		data.index = AMOUNT_LIMIT;
		getData(url_compliment, data);
	}

	$(window).scroll(function() {
		if($(window).scrollTop() + $(window).height() > $(document).height() - 100 && scroll_flag) {
			console.log("(on scroll) near bottom!");
			/* Stop detecting scroll bottom */
			scroll_flag = false;
			/* Retreive more data */
			console.log("(on scroll) search value: " + $("#icon_prefix").val());
			if ($("#icon_prefix").val() == "") {
				console.log("(on scroll) search empty");
				getData(url_compliment, data);
			} else {
				console.log("(on scroll) search NOT empty");
				search($('#icon_prefix').val());
			}
		}
	});

	function getData(url_compliment, data) {
		console.log("(getData):" + data.section + " url compliment:" + url_compliment + " url search:" + url_search);
		$.post(BASE_URL + url_compliment, data)
		.done(function(response) {
			console.log("data carregado: " + response.success + response.type + response.section);
			//console.log("data carregado: " + response.index + response.amount + response.section);
			showData(response);
			 /* increment index */
			data.index = data.index + AMOUNT_LIMIT;
			console.log('on done, index:' + data.index);
		})
		.always(function(response){
			/* allow scroll bottom detection again */
			scroll_flag = true;
			console.log('on always, scroll flag:' + scroll_flag);
		});
	}

	function showData(response) {
		/* load data into html */
		switch(type) {
			case 'photos':
				showPhotos(response);
				localStorage['medias'] = JSON.stringify(medias);
				break;
			case 'negatives':
				showNegatives(response);
				localStorage['medias'] = JSON.stringify(medias);
				break;
			case 'slides':
				showSlides(response);
				localStorage['medias'] = JSON.stringify(medias);
				break;
			case 'writings':
				showWritings(response);
				break;
			case 'sections':
				showSectionWritings(response);
				localStorage['medias'] = JSON.stringify(medias);
				break;
			default:
				showPhotos(response);
		}
	}

	function showPhotos(response) {
		$.each(response.photos, function(index, photo) {
			row.append(
				"<div class=\"col s6 m3\">" +
				"<a href=" + BASE_URL + "photo/" + photo.id + ">" +
					"<div class=\"card hoverable\">" +
	            "<div class=\"card-image\">" +
	              "<img src=" + "img/photos/front/thumbnails/" + photo.code +".jpg>" +
	              "<span class=\"card-title\">" + photo.code + "</span>" +
	            "<\/div>" +
	            "<div class=\"card-content\">" +
	              "<p class=\"black-text\">" + photo.title + "</p>" +
	            "</div>" +
		        "</div>" +
		        "</a>" +
				"</div>"
				);
			// add object to the array medias.
			console.log('midias: ' + medias.push(photo));
    	});
	}

	function showNegatives(response) {
		var row = $('.photo-content');
		$.each(response.negatives, function(index, negative) {
			row.append(
				"<div class=\"col s6 m3\">" +
				"<a href=" + BASE_URL + "negative/" + negative.id + ">" +
					"<div class=\"card hoverable\">" +
	            "<div class=\"card-image\">" +
	              "<img src=" + "img/photos/front/thumbnails/004.jpg" + ">" +
	              "<span class=\"card-title\">" + negative.code + "</span>" +
	            "<\/div>" +
	            "<div class=\"card-content\">" +
	              "<p class=\"black-text\">" + negative.collection + "</p>" +
	            "</div>" +
		        "</div>" +
		        "</a>" +
				"</div>"
				);
			console.log('midias: ' + medias.push(negative));
    	});
	}

	function showSlides(response) {
		var row = $('.photo-content');
		$.each(response.slides, function(index, slide) {
			row.append(
				"<div class=\"col s6 m3\">" +
				"<a href=" + BASE_URL + "slide/" + slide.id + ">" +
					"<div class=\"card hoverable\">" +
	            "<div class=\"card-image\">" +
	              "<img src=" + "img/photos/front/thumbnails/004.jpg" + ">" +
	              "<span class=\"card-title\">" + slide.code + "</span>" +
	            "<\/div>" +
	            "<div class=\"card-content\">" +
	              "<p class=\"black-text\">" + slide.description + "</p>" +
	            "</div>" +
		        "</div>" +
		        "</a>" +
				"</div>"
				);
			console.log('midias: ' + medias.push(slide));
    	});
	}

	function showWritings(response) {
		var row = $('.photo-content');
		var count = 0;
		$.each(response.writings, function(index, writing) {
			count ++;
			row.append(
				"<div class=\"col s6 m3\">" +
				"<a href=" + BASE_URL + "gsection/" + encodeURI(writing.section) + ">" +
					"<div class=\"card hoverable\"  style=\"background-color: #0a0c13a1;\">" +
	            "<div class=\"card-image\" style=\"padding: 23%;\">" +
	              "<img src=" + "img/helper/folder3.png" + ">" +
	              "<span class=\"card-title\">" + writing.section + "</span>" +
	            "<\/div>" +
	            "<div class=\"card-content white\">" +
	              "<p class=\"black-text\"> Pasta " + count + "</p>" +
	            "</div>" +
		        "</div>" +
		        "</a>" +
				"</div>"
				);
			//console.log('midias: ' + medias.push(writing));
    	});
	}

	function showSectionWritings(response) {
		var row = $('.photo-content');
		$.each(response.writings, function(index, writing) {
			row.append(
				"<div class=\"col s6 m3\">" +
				"<a href=" + BASE_URL + "writing/" + writing.id + ">" +
					"<div class=\"card hoverable\">" +
	            "<div class=\"card-image\">" +
	              "<img src=" + "http://localhost/gallery/public/img/photos/front/thumbnails/004.jpg" + ">" +
	              "<span class=\"card-title\">" + writing.code + "</span>" +
	            "<\/div>" +
	            "<div class=\"card-content\">" +
	              "<p class=\"black-text\">" + writing.description + "</p>" +
	            "</div>" +
		        "</div>" +
		        "</a>" +
				"</div>"
				);
			console.log('midias: ' + medias.push(writing));
    	});
	}

	function clearData() {
		row.empty();
	}

	/** Stop key enter from reloading page **/
	$('#search_form').submit(function(){
		return false;
	});

	/** at each key typed reset all the data related variables **/
	$('#icon_prefix').keyup(function() {
		clearData();
		medias.length = 0;
		localStorage.removeItem("medias");
		data.index = AMOUNT_LIMIT;
		search(this.value);
	});

	/** working, but should use another index type (change on Controller index too) **/
	function search(text) {
		data.text = text;
		console.log("text: " + data.text);
		if (search_post) {
			search_post.abort();
			console.log("search abortted");
		}
		search_post = $.post(BASE_URL + url_search, data)
		.done(function(response) {
			console.log("data carregado: " + response);
			showData(response);
			data.index = data.index + AMOUNT_LIMIT; // increment index.
			console.log('on done, index:' + data.index);
		})
		.always(function(response){
			search_post = false; // flag no post going on.
			scroll_flag = true; // allow scroll detection again.
			console.log('on always, scroll flag:' + scroll_flag);
			console.log("search response: " + response);
		});
	}

	/** Load data and scroll control**/

	/** Responsiveness **/
	if (screen.width < 993) {
		console.log("screen width:" + screen.width);
		$(".brand-logo").hide();
		$(".second-bar").show();
	} else {
		console.log("screen width not included:" + screen.width);
		$(".brand-logo").show();
		$(".second-bar").hide();
	}

	if (screen.width < 768) {
		console.log("screen width:" + screen.width);
		$(".card").removeClass("horizontal");
	}
});
